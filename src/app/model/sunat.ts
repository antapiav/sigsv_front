export class Sunat {

    constructor(
      public ruc?: string,
      public razon_social?: string,
      public ciiu?: string,
      public fecha_actividad?: string,
      public contribuyente_condicion?: string,
      public contribuyente_tipo?: string,
      public contribuyente_estado?: string,
      public nombre_comercial?: string,
      public fecha_inscripcion?: string,
      public domicilio_fiscal?: string,
      public sistema_emision?: string,
      public sistema_contabilidad?: string,
      public actividad_exterior?: string,
      public emision_electronica?: string,
      public fecha_inscripcion_ple?: string,
      public fecha_baja?: string
      ) {
  
    }
  
  }