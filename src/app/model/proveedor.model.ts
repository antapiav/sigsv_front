import { Multitabla } from './multitabla.model';
import { ProveedorCategoria } from './proveedor-categoria.model';

export class Proveedor {

    constructor(
      public idProveedor?: number,
      public numDocumento?: string,
      public nombre?: string,
      public direccion?: string,
      public contacto?: string,
      public telefono?: string,
      public email?: string,
      public indActivo?: boolean,
      public tipoDocumento?: Multitabla,
      public proveedorCategoria?: ProveedorCategoria
      ) {
  
    }
  }