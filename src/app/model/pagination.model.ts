import { AppConstants } from "../util/app.constants";
import { AppUtils } from "../util/app.utils";
import { ElementRef } from "@angular/core";

export class Pagination {

    constructor(
        public page?: number,
        public limit?: number,
        public filters?: Array<any>,
        public orders?: Array<any>,
        public total?: number,
        public amountPage?: number,
        public amountTotal?: number,
        public data?: any,
        public config?: any,
        public loading?: boolean) {
        this.page = page ? page : AppConstants.APP_PAGINATION_DEFAULT_PAGE;
        this.limit = limit ? limit : AppConstants.APP_PAGINATION_DEFAULT_LIMIT;
    }

    setData(data: any) {

    }

    setConfig(config: any): void {
        this.total = config ? config.total : 0;
        this.amountPage = config ? config.importePagina : 0;
        this.amountTotal = config ? config.importeTotal : 0;
    }

    isEmpty(): boolean {
        return !this.total || this.total == 0;
    }

    getNroRecord(index: number): number {
        return (index + 1) + (this.page - 1) * this.limit;
    }

    getMinRecord(): number {
        return ((this.limit * this.page) - this.limit) + 1;
    }

    getMaxRecord(): number {
        let max = this.limit * this.page;
        if (max > this.total) {
            max = this.total;
        }
        return max;
    }

    getTotalRecords(): number {
        return this.total || 0;
    }

    getFirstPage(): boolean {
        return this.page <= 1;
    }

    getLastPage(): boolean {
        return this.limit * this.page >= this.total;
    }

    getTotalPages(): number {
        return Math.ceil(this.total / this.limit) || 0;
    }

    getAmountPage(): number {
        return this.amountPage || 0;
    }

    getAmountTotal(): number {
        return this.amountTotal || 0;
    }

    onFirst(): void {
        this.page = 1;
    }

    onPrev(): void {
        this.page--;
    }

    onPage(page: number): void {
        this.page = page || 1;
    }

    onNext(): void {
        this.page++;
    }

    onLast(): void {
        this.page = this.getTotalPages();
    }

    showLoading(): void {
        this.loading = true;
    }

    hideLoading(): void {
        this.loading = false;
    }

    onFocus(id: string, element: ElementRef): void {
        AppUtils.onElementFocus(id, element);
    }

    createFilters(): void {
        this.filters = this.filters || new Array();
    }

    insertFilter(name: string, operator: string, value: any): void {
        let filter: any = this.filters.find(filter => filter && filter.name == name && filter.operator == operator);

        if (!filter) {
            filter = {};
            this.filters.push(filter);
        }
        filter.name = name;
        filter.operator = operator;
        filter.value = value;
    }

    removeFilter(name: string): void {
        this.filters = this.filters.filter(filter => filter && filter.name != name);
    }

    clearFilters(): void {
        this.filters = new Array();
    }

    joinFilters(): string {
        return this.filters && this.filters.length > 0 ? this.filters.map(filter => filter.name + ',' + filter.operator + ',' + filter.value).join(';') : null;
    }

    insertFilterEquals(name: string, value: any): void {
        if (typeof value !== 'string' || (typeof value === 'string' && value !== '0000000')) {
            this.insertFilter(name, 'equals', value);
        }
    }

    insertFilterIn(name: string, ...value: any[]): void {
        this.insertFilter(name, 'in', AppUtils.concatCriterioIn(value));
    }

    insertFilterNotIn(name: string, ...value: any[]): void {
        this.insertFilter(name, 'notin', AppUtils.concatCriterioIn(value));
    }

    insertFilterRegex(name: string, value: any): void {
        this.insertFilter(name, 'regex', value);
    }

    applyFilterLeq(name: string, value: any): void {
        if (value) {
            if (typeof value === 'string' && value === '0000000') {
                this.removeFilter(name);
            } else {
                this.insertFilter(name, 'leq', value);
            }
        } else {
            this.removeFilter(name);
        }
    }

    applyFilterGeq(name: string, value: any): void {
        if (value) {
            if (typeof value === 'string' && value === '0000000') {
                this.removeFilter(name);
            } else {
                this.insertFilter(name, 'geq', value);
            }
        } else {
            this.removeFilter(name);
        }
    }

    applyFilterEquals(name: string, value: any): void {
        if (value) {
            if (typeof value === 'string' && value === '0000000') {
                this.removeFilter(name);
            } else {
                this.insertFilter(name, 'equals', value);
            }
        } else {
            this.removeFilter(name);
        }
    }

    applyFilterNotEquals(name: string, value: any): void {
        if (value) {
            if (typeof value === 'string' && value === '0000000') {
                this.removeFilter(name);
            } else {
                this.insertFilter(name, 'notequals', value);
            }
        } else {
            this.removeFilter(name);
        }
    }

    applyFilterLike(name: string, value: any): void {
        if (value) {
            this.insertFilter(name, 'like', value);
        } else {
            this.removeFilter(name);
        }
    }

    applyFilterIn(name: string, ...value: any[]): void {
        if (value) {
            this.insertFilter(name, 'in', AppUtils.concatCriterioIn(value));
        } else {
            this.removeFilter(name);
        }
    }

    applyFilterNotIn(name: string, ...value: any[]): void {
        if (value) {
            this.insertFilter(name, 'notin', AppUtils.concatCriterioIn(value));
        } else {
            this.removeFilter(name);
        }
    }

    applyFilterRegex(name: string, value: any): void {
        if (value) {
            if (typeof value === 'string' && value === '0000000') {
                this.removeFilter(name);
            } else {
                this.insertFilter(name, 'regex', value);
            }
        } else {
            this.removeFilter(name);
        }
    }

    removeFilterEquals(name: string): void {
        this.removeFilter(name);
    }
}