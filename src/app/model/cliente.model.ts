export class Cliente {

  constructor(
    public idCliente?: number,
    public dni?: string,
    public nombre?: string,
    public apPaterno?: string,
    public apMaterno?: string,
    public direccion?: string,
    public contacto?: string,
    public telefono?: string,
    public email?: string,
    public indActivo?: boolean
    ) {

  }

}
