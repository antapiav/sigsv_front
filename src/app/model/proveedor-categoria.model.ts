export class ProveedorCategoria {

    constructor(
        public idProveedorCategoria?: number,
        public nombre?: string,
        public detalle?: string,
        public indActivo?: boolean
      ) {
  
    }
  }