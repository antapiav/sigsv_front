export class Multitabla {

    constructor(
       public idCodigo?: string,
       public idTabla?: string,
       public idItem?: string,
       public descripcion?: string,
       public descripcionValor?: string,
       public indActivo?: boolean
      ) {
  
    }
  }