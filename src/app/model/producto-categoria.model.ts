export class ProductoCategoria {

    constructor(
        public idProductoCategoria?: number,
        public nombre?: string,
        public detalle?: string,
        public indActivo?: boolean
      ) {
  
    }
  }