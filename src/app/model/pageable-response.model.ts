export class PageableResponse<T> {

    constructor(
      public number?: number,
      public totalElements?: number,
      public content?: T) { }
  }