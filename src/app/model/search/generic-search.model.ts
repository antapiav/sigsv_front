export class GenericSearch {

    constructor(
        public tipo?: string,
        public dato?: string,
        public indActivo?: boolean
        ) { }
}