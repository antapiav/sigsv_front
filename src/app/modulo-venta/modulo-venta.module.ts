import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { SigsvModuloVentaRoutes } from './modulo-venta.route';

import { NuevaVentaComponent } from './nueva/nueva-venta.component';
import { ListaVentaComponent } from './lista/lista-venta.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        ModalModule,
        CollapseModule,
        BsDropdownModule,

        SigsvModuloVentaRoutes
    ],
    providers: [],
    declarations: [
        NuevaVentaComponent,
        ListaVentaComponent
    ]
  })
  export class ModuloVentaModule { }