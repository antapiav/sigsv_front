import { Route } from '@angular/router';

import { NuevaVentaComponent } from './nueva-venta.component';

export const nuevaVentaRoute: Route = {
    path: 'venta', component: NuevaVentaComponent,
    data: {
        title: 'Modulo de Ventas - Nueva Venta',
        breadcumb: ['SIGSV', 'Ventas', 'Nueva Venta']
    }
};