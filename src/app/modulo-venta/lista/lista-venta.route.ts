import { Route } from '@angular/router';

import { ListaVentaComponent } from './lista-venta.component';

export const listaVentaRoute: Route = {
    path: 'lst-venta', component: ListaVentaComponent,
    data: {
        title: 'Modulo de Ventas - Lista de Ventas',
        breadcumb: ['SIGSV', 'Ventas', 'Lista de Ventas']
    }
};