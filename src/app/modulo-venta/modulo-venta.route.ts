import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { nuevaVentaRoute } from './nueva/nueva-venta.route';
import { listaVentaRoute } from './lista/lista-venta.route';

const MODULO_VENTA_ROUTES = [
    nuevaVentaRoute,
    listaVentaRoute
];

export const moduloVentaRoutes: Routes = MODULO_VENTA_ROUTES;

export const SigsvModuloVentaRoutes: ModuleWithProviders = RouterModule.forChild(moduloVentaRoutes);