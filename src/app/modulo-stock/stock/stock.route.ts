import { Route } from '@angular/router';

import { StockComponent } from './stock.component';

export const stockRoute: Route = {
    path: 'stock', component: StockComponent,
    data: {
        title: 'Modulo Stock',
        breadcumb: ['SIGSV', 'Stock', 'Operaciones']
    }
};