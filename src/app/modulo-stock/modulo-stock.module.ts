import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { SigsvModuloStockRoutes } from './modulo-stock.route';

import { StockComponent } from './stock/stock.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        ModalModule,
        CollapseModule,
        BsDropdownModule,

        SigsvModuloStockRoutes
    ],
    providers: [],
    declarations: [
        StockComponent
    ]
  })
  export class ModuloStockModule { }