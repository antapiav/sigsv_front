import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { stockRoute } from './stock/stock.route';

const MODULO_STOCK_ROUTES = [
    stockRoute
];

export const moduloStockRoutes: Routes = MODULO_STOCK_ROUTES;

export const SigsvModuloStockRoutes: ModuleWithProviders = RouterModule.forChild(moduloStockRoutes);