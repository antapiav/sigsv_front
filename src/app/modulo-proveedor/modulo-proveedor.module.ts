import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { NgxSelectModule } from 'ngx-select-ex';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';
import { CommonsModule } from '../commons/commons.module';

import { SigsvModuloProveedorRoutes } from './modulo-proveedor.route';

import { ProveedorComponent } from './proveedor/proveedor.component';
import { ProveedorCategoriaComponent } from './proveedor-categoria/proveedor-categoria.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgSelectModule,
        ReactiveFormsModule,
        AccordionModule,
        CommonsModule,
        NgxSelectModule,
        SigsvModuloProveedorRoutes
    ],
    providers: [],
    declarations: [
        ProveedorComponent,
        ProveedorCategoriaComponent
    ]
  })
  export class ModuloProveedorModule { }