import { Component, OnInit, ElementRef } from '@angular/core';
import { GenericSearch } from 'src/app/model/search/generic-search.model';
import { Pagination } from 'src/app/model/pagination.model';
import { Proveedor } from 'src/app/model/proveedor.model';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { ProveedorService } from 'src/app/services/proveedor/proveedor.service';
import { AppConstants } from 'src/app/util/app.constants';
import { ProveedorCategoriaService } from 'src/app/services/proveedor/proveedor-categoria.service';
import { catchError, finalize } from 'rxjs/operators';
import { ProveedorCategoria } from 'src/app/model/proveedor-categoria.model';
import { proveedorCategoriaRoute } from '../proveedor-categoria/proveedor-categoria.route';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { AppMessages } from 'src/app/services/app.messages';
import { ModalDetailProveedorComponent } from './detail/modal-detail-proveedor.component';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {


  public genericSearch: GenericSearch;
  public genericPagination: Pagination;//paginationProductoCategoria
  public lstProveedor: Array<Proveedor>;
  public lstProveedorCategoria: Array<ProveedorCategoria>;
  public proveedorCategoria: ProveedorCategoria;

  public estadoAcordion = true;
  public headPage: any;

  public estadoCbProveedorCategoria: string;
  public estadoEmtyProveedorCategoria: string;

  public selectTipoDocumento = [
    {id: 'nombre', text: 'Nombre'},
    {id: 'codigo', text: 'Código'}
  ]

  constructor(
    private proveedorService: ProveedorService,
    private proveedorCategoriaService: ProveedorCategoriaService,
    private element: ElementRef,
    private commonsService: CommonsService
  ) { 
    this.genericSearch = new GenericSearch();
    this.genericPagination = new Pagination();
    this.proveedorCategoria = new ProveedorCategoria();
    this.genericPagination.createFilters();
    this.lstProveedor = new Array();
  }

  ngOnInit(): void {
    this.estadoCbProveedorCategoria = AppConstants.Field.STRING_CARGANDO;
    this.estadoEmtyProveedorCategoria = AppConstants.Field.STRING_CARGANDO;
    this.genericSearch.tipo = AppConstants.Field.STRING_VACIO;
    this.reqLstProveedorCategoria(AppConstants.Field.STRING_VACIO);
    this.reqLstPaginado(this.genericPagination.page);
  }

  clearFilter(){
    this.genericPagination.clearFilters();
  }

  btnBuscarLst(){
    this.genericPagination.applyFilterEquals(this.genericSearch.tipo, this.genericSearch.dato);
    this.reqLstPaginado();
  }

  btnLimpiar(){
    this.proveedorCategoria = new ProveedorCategoria();
    this.genericSearch.tipo = AppConstants.Field.STRING_CODIGO;
    this.genericSearch.dato = AppConstants.Field.STRING_VACIO;
    this.reqLstPaginado();
  }

  btnNuevo(){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_REGISTRAR_PROVEEDOR,
      operation: AppConstants.Modal.OP_SAVE,
    };
    this.commonsService.openModalLarge(
      ModalDetailProveedorComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.genericPagination.clearFilters();
          this.reqLstPaginado(this.genericPagination.page);
        }
      });
  }

  btnDescargarExcel(){}

  btnRefreshLst(){
    this.genericPagination.clearFilters();
    this.reqLstPaginado(this.genericPagination.page);
  }

  btnDetalle(proveedor: Proveedor){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_DETALLE_PROVEEDOR,
      operation: AppConstants.Modal.OP_DETAIL,
      options: proveedor
    };
    this.commonsService.openModalLarge(
      ModalDetailProveedorComponent,
      params)
      .subscribe((response) => {
      });
  }

  btnEditar(proveedor: Proveedor){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_ACTUALIZAR_PROVEEDOR,
      operation: AppConstants.Modal.OP_UPDATE,
      options: proveedor
    };
    this.commonsService.openModalLarge(
      ModalDetailProveedorComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.genericPagination.clearFilters();
          this.reqLstPaginado(this.genericPagination.page);
        }
      });
  }
  
  async btnBaja(proveedor: Proveedor){
    const  a = await this.commonsService.openModalConfirmQuestion();
    if(a.isConfirmed){
      this.commonsService.openModalLoading();
    this.proveedorService.indActivoProveedor(proveedor.idProveedor)
        .pipe(
          catchError(error => {
            this.commonsService.openModalErrorServer();
            throw error;
          }),
          finalize(() => {this.commonsService.closeModalLoading()}))
          .subscribe((data) => {
            const response: GenericResponse<any> = data.body;
            switch(response.code){
              case 200:
                this.reqLstPaginado();
                this.commonsService.openModalExitoOperation();
                break;
              default:
                this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
            }
        });
    }
  }

  reqLstPaginado(page?: number){
    this.genericPagination.showLoading();
    this.genericPagination.onPage(page);
    this.proveedorService.getAll(this.genericPagination, (this.proveedorCategoria.idProveedorCategoria === null 
      || this.proveedorCategoria.idProveedorCategoria === undefined) 
      ? AppConstants.Field.STRING_VACIO : String(this.proveedorCategoria.idProveedorCategoria))
    .pipe(
      catchError(error => {
        console.log('error occured:', error);
        throw error;
      }),
      finalize(() => {
        this.genericPagination.onFocus(AppConstants.ID_TABLE_LST_PROVEEDOR, this.element);
        this.genericPagination.hideLoading();
      })
    ).subscribe((data) => {
        this.lstProveedor = data.body.content;
        const total = data.body.totalElements;
        const pagina = (data.body.number)+1;
        this.headPage = {pagina, total};
        this.genericPagination.setConfig(this.headPage);
      });
  }

  reqLstProveedorCategoria(value){
    this.estadoEmtyProveedorCategoria = AppConstants.Field.STRING_CARGANDO;
    this.proveedorCategoriaService.getLstProveedorCategoria(value)
    .pipe(
      catchError(error => {
        this.estadoCbProveedorCategoria=AppConstants.Field.STRING_ERROR_CARGA;
        this.estadoEmtyProveedorCategoria=AppConstants.Field.STRING_ERROR_CARGA;
        console.log('error occured:', error);
        throw error;
      }),
      finalize(() => {})
    ).subscribe((data) => {
      this.lstProveedorCategoria = data.body.body;
      this.estadoCbProveedorCategoria=AppConstants.Field.STRING_SELECCIONE_CATEGORIA;
      if(data.body.body.length == 0){
        this.estadoEmtyProveedorCategoria = AppConstants.Field.STRING_SIN_RESULTADOS;
      }
      });
  }

}
