import { Component, OnInit, Renderer2, HostListener, Output, EventEmitter, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AppConstants } from 'src/app/util/app.constants';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { SunatReniecService } from 'src/app/services/sunat-reniec/sunat-reniec.service';
import { catchError, finalize, timeout } from 'rxjs/operators';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { Multitabla } from 'src/app/model/multitabla.model';
import { AppMessages } from 'src/app/services/app.messages';
import { ProveedorCategoria } from 'src/app/model/proveedor-categoria.model';
import { ProveedorCategoriaService } from 'src/app/services/proveedor/proveedor-categoria.service';
import { Proveedor } from 'src/app/model/proveedor.model';
import { ProveedorService } from 'src/app/services/proveedor/proveedor.service';

@Component({
  selector: 'app-modal-detail-proveedor',
  templateUrl: './modal-detail-proveedor.component.html',
  styleUrls: ['./modal-detail-proveedor.component.css']
})
export class ModalDetailProveedorComponent implements OnInit {
  //variables entrada open modal
  public titulo: string;
  public operation: string;
  public options: any;
  //varaibles modal front
  public disable: boolean = false;
  public disableBtnRegistrar: boolean = false;
  public hideStatus: boolean = false;
  public nameBtnOperation: string;
  //variables propias
  public proveedorCategoria: ProveedorCategoria;
  public response: boolean = false;
  //varaibles form validator
  public proveedorForm: FormGroup;
  public statusValidateNumDocumento = false;
  public statusValidateNombre = false;
  public statusValidateDireccion = false;
  public statusValidateEmail = false;
  public statusValidateCategoria = false;

  public proveedor = new Proveedor;

  public selectTipoDocumento = [
    {id: 'nombre', text: 'Nombre'},
    {id: 'codigo', text: 'Código'}
  ]
  public lstTipoDocumento: Array<Multitabla>;
  public tipoDocumento: Multitabla;

  public txtSunatReniec: string;
  public sizeSunatReniec: number;
  public estadoCbProveedorCategoria: string;
  public estadoEmtyProveedorCategoria: string;

  public lstProveedorCategoria: Array<ProveedorCategoria>;

  constructor(
    public bsModalRef: BsModalRef,
    private proveedorService: ProveedorService,
    private proveedorCategoriaService: ProveedorCategoriaService,
    private commonsService: CommonsService,
    private sunatReniecService: SunatReniecService,
  ) {
    this.proveedorCategoria = new ProveedorCategoria();
    this.proveedor = new Proveedor();
    //this.proveedor.proveedorCategoria = new ProveedorCategoria();
    this.tipoDocumento = new Multitabla();
    this.lstTipoDocumento = new Array();
    this.txtSunatReniec = AppConstants.Field.STRING_SUNAT;
    this.sizeSunatReniec = AppConstants.Field.INT_SIZE_TXT_RUC;
  }

  ngOnInit() {
    this.tipoDocumento.idCodigo = "00402";
    this.estadoCbProveedorCategoria = AppConstants.Field.STRING_CARGANDO;
    this.estadoEmtyProveedorCategoria = AppConstants.Field.STRING_CARGANDO;
    this.formProveedor();
    setTimeout(() => {
      switch(this.operation){
        case AppConstants.Modal.OP_SAVE:
          this.hideStatus = false;
          this.disableBtnRegistrar = true;
          this.proveedor.indActivo = true;
          this.nameBtnOperation = AppConstants.Modal.BTN_REGISTRAR;
          this.proveedorForm.controls['indActivo'].disable();
          this.reqLstProveedorcategoria(AppConstants.Field.STRING_VACIO);
          break;
        case AppConstants.Modal.OP_UPDATE:
          this.getDetalleProveedor(this.options);
          this.hideStatus = false;
          this.disableBtnRegistrar = false;
          this.nameBtnOperation = AppConstants.Modal.BTN_ACTUALIZAR;
          break;
        case AppConstants.Modal.OP_DETAIL:
          this.disable = true;
          this.disableBtnRegistrar = true;
          this.getDetalleProveedor(this.options);
          this.hideStatus = true;
          this.proveedorForm.disable();
          break;
        default:
          this.hideStatus = false
      }
    });
  }

  formProveedor(){
      this.proveedorForm =new FormGroup({
      idProveedor: new FormControl({value: this.proveedor.idProveedor, disabled: this.hideStatus}),
      numDocumento: new FormControl({value: this.proveedor.numDocumento, disabled: this.hideStatus}, [Validators.required, Validators.minLength(this.sizeSunatReniec), Validators.maxLength(this.sizeSunatReniec)]),
      nombre: new FormControl({value: this.proveedor.nombre, disabled: this.hideStatus}, [Validators.required]),
      direccion: new FormControl({value: this.proveedor.direccion, disabled: this.hideStatus}, [Validators.required]),
      contacto: new FormControl({value: this.proveedor.contacto, disabled: this.hideStatus}),
      telefono: new FormControl({value: this.proveedor.telefono, disabled: this.hideStatus}),
      email: new FormControl({value: this.proveedor.email, disabled: this.hideStatus}, [Validators.email]),
      indActivo: new FormControl({value: this.proveedor.indActivo, disabled: this.hideStatus}),
      tipoDocumento: new FormControl({value: this.tipoDocumento.idCodigo, disabled: this.hideStatus}),
      categoria: new FormControl({value: this.proveedorCategoria.idProveedorCategoria, disabled: this.hideStatus}, [Validators.required])
    });
  }
  //validation formulario
  get f() { return this.proveedorForm.controls; }

  changeTipoDocumento(tipoDocumento){
    this.tipoDocumento.idCodigo = tipoDocumento;
    switch(this.tipoDocumento.idCodigo){
      case "00402":
        this.proveedor.numDocumento = AppConstants.Field.STRING_VACIO;
        this.txtSunatReniec  = AppConstants.Field.STRING_SUNAT;
        this.sizeSunatReniec = AppConstants.Field.INT_SIZE_TXT_RUC;
        this.proveedorForm.controls.numDocumento.setValidators([Validators.required, Validators.minLength(11), Validators.maxLength(11)]);
      break;
      case "00401":
        this.proveedor.numDocumento = AppConstants.Field.STRING_VACIO;
        this.txtSunatReniec  = AppConstants.Field.STRING_RENIEC;
        this.sizeSunatReniec = AppConstants.Field.INT_SIZE_TXT_DNI;
        this.proveedorForm.controls.numDocumento.setValidators([Validators.required, Validators.minLength(8), Validators.maxLength(8)]);
      break;
    }
  }

  validInput(nameTxt){
    switch(nameTxt){
      case "numDocumento":
        this.statusValidateNumDocumento = true;
        break;
      case "nombre":
        this.statusValidateNombre = true;
        break;
      case "direccion":
        this.statusValidateDireccion = true;
        break;
      case "categoria":
        this.statusValidateCategoria = true;
        break;
      case "email":
        this.statusValidateEmail = true;
        break;
    }
  }

  btnRegistrar() {
    this.proveedor.tipoDocumento = this.tipoDocumento;
    this.proveedor.proveedorCategoria = this.proveedorCategoria;
    if(!this.proveedorForm.invalid){
      this.commonsService.openModalLoading();
      this.proveedorService.postPutOperation(this.proveedor, this.operation)
        .pipe(
        catchError(error => {
          this.commonsService.openModalErrorServer();
          throw error;
        }),
        finalize(() => {this.commonsService.closeModalLoading()}))
        .subscribe((data) => {
          const response: GenericResponse<any> = data.body;
          switch(response.code){
            case 200:
              this.response = true;
              this.bsModalRef.hide();
              break;
            case 1:
              this.response = false;
              this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_OHOH_MODAL, response.message);
              break;
            default:
              this.response = false;
              this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
          }
        });
    }
  }

  getDataSunatReniec(){
    this.commonsService.openModalLoading();
    switch(this.tipoDocumento.idCodigo){
      case "00402":
        this.sunatReniecService.getDataSunat(this.proveedor.numDocumento)
        .pipe(
          timeout(60000),
          catchError(error => {
            this.commonsService.openModalErrorServer();
            throw error;
          }),
          finalize(() => {this.commonsService.closeModalLoading()}))
          .subscribe((data) => {
            if(data.body.code!=0){
              if(data.body.body != null){
                this.proveedor.numDocumento = data.body.body.ruc;
                this.proveedor.nombre = data.body.body.razon_social;
                this.proveedor.direccion = data.body.body.domicilio_fiscal;
              }else{
                this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_SEGURO_MODAL, AppMessages.NO_EXISTE_DOCUMENTO);
              }
            }else{
              this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_SEGURO_MODAL, data.body.message);
            }
          });
      break;
      case "00401":
        this.sunatReniecService.getDataReniec(this.proveedor.numDocumento)
        .pipe(
          timeout(60000),
          catchError(error => {
            this.commonsService.openModalErrorServer();
            throw error;
          }),
          finalize(() => {this.commonsService.closeModalLoading()}))
          .subscribe((data) => {
            if(data.body.code!=0){
              this.proveedor.numDocumento = data.body.body.dni;
              this.proveedor.nombre = data.body.body.nombre+" "+data.body.body.apPaterno+" "+data.body.body.apMaterno;
            }else{
              this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_SEGURO_MODAL, data.body.message);
            }
          });
      break;
    }
    
  }

  async getDetalleProveedor(proveedorCategoria: Proveedor){
    this.commonsService.openModalLoading();
    (await this.proveedorService.getProveedor(proveedorCategoria.idProveedor))
      .pipe(
      catchError(error => {
        this.commonsService.openModalErrorServer();
        throw error;
      }),
      finalize(() => {}))
      .subscribe((data) => {
        this.proveedor = data.body.body;
        this.reqLstProveedorcategoria(proveedorCategoria.proveedorCategoria.nombre);
        this.formProveedor();
      });
  }

  async reqLstProveedorcategoria(value){
    this.estadoEmtyProveedorCategoria = AppConstants.Field.STRING_CARGANDO;
    this.proveedorCategoriaService.getLstProveedorCategoria(value)
    .pipe(
      catchError(error => {
        this.estadoCbProveedorCategoria=AppConstants.Field.STRING_ERROR_CARGA;
        this.estadoEmtyProveedorCategoria=AppConstants.Field.STRING_ERROR_CARGA;
        console.log('error occured:', error);
        throw error;
      }),
      finalize(() => {
        this.commonsService.closeModalLoading();
      }))
      .subscribe((data) => {
      this.lstProveedorCategoria = data.body.body;
      if(this.lstProveedorCategoria.length == 1){
        this.proveedorCategoria = this.lstProveedorCategoria[0]
      }
      this.estadoCbProveedorCategoria=AppConstants.Field.STRING_SELECCIONE_CATEGORIA;
      if(data.body.body.length == 0){
        this.estadoEmtyProveedorCategoria = AppConstants.Field.STRING_SIN_RESULTADOS;
      }
      this.commonsService.closeModalLoading();
      });
  }

}