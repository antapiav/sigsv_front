import { Route } from '@angular/router';

import { ProveedorComponent } from './proveedor.component';

export const proveedorRoute: Route = {
    path: 'proveedor', component: ProveedorComponent,
    data: {
        title: 'Modulo Proveedor - Registro de Proveedor',
        breadcumb: ['SIGSV', 'Proveedor', 'Registro de Proveedor']
    }
};