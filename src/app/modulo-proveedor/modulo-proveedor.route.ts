import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { proveedorRoute } from './proveedor/proveedor.route';
import { proveedorCategoriaRoute } from './proveedor-categoria/proveedor-categoria.route';

const MODULO_PROVEEDOR_ROUTES = [
    proveedorRoute,
    proveedorCategoriaRoute
];

export const moduloProveedorRoutes: Routes = MODULO_PROVEEDOR_ROUTES;

export const SigsvModuloProveedorRoutes: ModuleWithProviders = RouterModule.forChild(moduloProveedorRoutes);