import { Route } from '@angular/router';

import { ProveedorCategoriaComponent } from './proveedor-categoria.component';

export const proveedorCategoriaRoute: Route = {
    path: 'proveedor-categoria', component: ProveedorCategoriaComponent,
    data: {
        title: 'Modulo Proveedor - Registro de Categorias de Proveedor',
        breadcumb: ['SIGSV', 'Categoria Proveedor', 'Registro de Categorias de Proveedor']
    }
};