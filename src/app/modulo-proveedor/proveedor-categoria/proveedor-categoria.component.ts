import { Component, OnInit, ElementRef, ɵConsole } from '@angular/core';

import { GenericSearch } from 'src/app/model/search/generic-search.model';
import { Pagination } from 'src/app/model/pagination.model';
import { ProveedorCategoria } from 'src/app/model/proveedor-categoria.model';
import { AppConstants } from 'src/app/util/app.constants';
import { ProveedorCategoriaService } from 'src/app/services/proveedor/proveedor-categoria.service';
import { catchError, finalize } from 'rxjs/operators';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { AppMessages } from 'src/app/services/app.messages';
import { ModalDetailProveedorCategoriaComponent } from './detail/modal-detail-proveedor-categoria.component';

@Component({
  selector: 'app-proveedor-categoria',
  templateUrl: './proveedor-categoria.component.html',
  styleUrls: ['./proveedor-categoria.component.css']
})
export class ProveedorCategoriaComponent implements OnInit {

  public searchProveedorCategoria: GenericSearch;
  public paginationProveedorCategoria: Pagination;
  public lstProveedorCategoria: Array<ProveedorCategoria>;

  public estadoAcordion = true;
  public headPage: any;

  constructor(
    private proveedorCategoriaService: ProveedorCategoriaService,
    private element: ElementRef,
    private commonsService: CommonsService
  ) { 
    this.searchProveedorCategoria = new GenericSearch();
    this.paginationProveedorCategoria = new Pagination();
    this.paginationProveedorCategoria.createFilters();
    this.lstProveedorCategoria = new Array();
  }

  ngOnInit(): void {
    this.searchProveedorCategoria.tipo = AppConstants.Field.STRING_NOMBRE;
    this.paginationProveedorCategoria.clearFilters();
    this.reqLstProveedorCategoriaPaginado(this.paginationProveedorCategoria.page);
  }

  btnBuscarLstProveedorCategoria(){
    this.paginationProveedorCategoria.applyFilterEquals(this.searchProveedorCategoria.tipo, this.searchProveedorCategoria.dato);
    this.reqLstProveedorCategoriaPaginado();
  }

  btnLimpiar(){
    this.searchProveedorCategoria.tipo = AppConstants.Field.STRING_NOMBRE;
    this.searchProveedorCategoria.dato = AppConstants.Field.STRING_VACIO;
    this.reqLstProveedorCategoriaPaginado();
  }

  btnNuevoProveedorCategoria(){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_REGISTRAR_PROVEEDOR_CATEGORIA,
      operation: AppConstants.Modal.OP_SAVE,
    };
    this.commonsService.openModalLarge(
      ModalDetailProveedorCategoriaComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.paginationProveedorCategoria.clearFilters();
          this.reqLstProveedorCategoriaPaginado(this.paginationProveedorCategoria.page);
        }
      });
  }

  btnDescargarExcel(){}

  btnRefreshLstProveedorCategoria(){
    this.paginationProveedorCategoria.clearFilters();
    this.reqLstProveedorCategoriaPaginado(this.paginationProveedorCategoria.page);
  }

  btnDetalleProveedorCategoria(proveedorCategoria: ProveedorCategoria){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_DETALLE_PROVEEDOR_CATEGORIA,
      operation: AppConstants.Modal.OP_DETAIL,
      options: proveedorCategoria
    };
    this.commonsService.openModalLarge(
      ModalDetailProveedorCategoriaComponent,
      params)
      .subscribe((response) => {
      });
  }

  btnEditarProveedorCategoria(proveedorCategoria: ProveedorCategoria){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_ACTUALIZAR_PROVEEDOR_CATEGORIA,
      operation: AppConstants.Modal.OP_UPDATE,
      options: proveedorCategoria
    };
    this.commonsService.openModalLarge(
      ModalDetailProveedorCategoriaComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.paginationProveedorCategoria.clearFilters();
          this.reqLstProveedorCategoriaPaginado(this.paginationProveedorCategoria.page);
        }
      });
  }

  async btnBajaProveedorCategoria(proveedorCategoria: ProveedorCategoria){
    const  a = await this.commonsService.openModalConfirmQuestion();
    if(a.isConfirmed){
      this.commonsService.openModalLoading();
    this.proveedorCategoriaService.indActivoProveedorCategoria(proveedorCategoria.idProveedorCategoria)
        .pipe(
          catchError(error => {
            this.commonsService.openModalErrorServer();
            throw error;
          }),
          finalize(() => {this.commonsService.closeModalLoading()}))
        .subscribe((data) => {
            const response: GenericResponse<any> = data.body;
            switch(response.code){
              case 200:
                this.reqLstProveedorCategoriaPaginado();
                this.commonsService.openModalExitoOperation();
                break;
              default:
                this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
            }
        });
    }
  }

  reqLstProveedorCategoriaPaginado(page?: number){
    this.paginationProveedorCategoria.showLoading();
    this.paginationProveedorCategoria.onPage(page);
    this.proveedorCategoriaService.getAll(this.paginationProveedorCategoria)
    .pipe(
      catchError(error => {
        console.log('error occured:', error);
        throw error;
      }),
      finalize(() => {
        this.paginationProveedorCategoria.onFocus(AppConstants.ID_TABLE_LST_PROVEEDOR_CATEGORIA, this.element);
        this.paginationProveedorCategoria.hideLoading();
      })
    ).subscribe((data) => {
        this.lstProveedorCategoria = data.body.content;
        const total = data.body.totalElements;
        const pagina = (data.body.number)+1;
        this.headPage = {pagina, total};
        this.paginationProveedorCategoria.setConfig(this.headPage);
      });
  }

}
