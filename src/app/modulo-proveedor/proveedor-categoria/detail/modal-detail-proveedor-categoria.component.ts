import { Component, OnInit, Renderer2, HostListener, Output, EventEmitter, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AppConstants } from 'src/app/util/app.constants';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { SunatReniecService } from 'src/app/services/sunat-reniec/sunat-reniec.service';
import { catchError, finalize, timeout } from 'rxjs/operators';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { AppMessages } from 'src/app/services/app.messages';
import { ProveedorCategoria } from 'src/app/model/proveedor-categoria.model';
import { ProveedorCategoriaService } from 'src/app/services/proveedor/proveedor-categoria.service';

@Component({
  selector: 'app-modal-detail-proveedor-categoria',
  templateUrl: './modal-detail-proveedor-categoria.component.html',
  styleUrls: ['./modal-detail-proveedor-categoria.component.css']
})
export class ModalDetailProveedorCategoriaComponent implements OnInit {
  //variables entrada open modal
  public titulo: string;
  public operation: string;
  public options: any;
  //varaibles modal front
  public disable: boolean = false;
  public disableBtnRegistrar: boolean = false;
  public hideStatus: boolean = false;
  public nameBtnOperation: string;
  //variables propias
  public proveedorCategoria: ProveedorCategoria;
  public response: boolean = false;
  //varaibles form validator
  public proveedorCategoriaForm: FormGroup;
  public statusValidateNombre = false;
  public statusValidateDetalle= false;


  constructor(
    public bsModalRef: BsModalRef,
    private proveedorCategoriaService: ProveedorCategoriaService,
    private commonsService: CommonsService
  ) {
    this.proveedorCategoria = new ProveedorCategoria();
  }

  ngOnInit() {
    this.formProveedorCategoria();
    setTimeout(() => {
      switch(this.operation){
        case AppConstants.Modal.OP_SAVE:
          this.hideStatus = false;
          this.disableBtnRegistrar = true;
          this.proveedorCategoria.indActivo = true;
          this.nameBtnOperation = AppConstants.Modal.BTN_REGISTRAR;
          this.proveedorCategoriaForm.controls['indActivo'].disable();
          break;
        case AppConstants.Modal.OP_UPDATE:
          this.getDetalleProveedorCategoria(this.options.idProveedorCategoria);
          this.hideStatus = false;
          this.disableBtnRegistrar = false;
          this.nameBtnOperation = AppConstants.Modal.BTN_ACTUALIZAR;
          break;
        case AppConstants.Modal.OP_DETAIL:
          this.disable = true;
          this.disableBtnRegistrar = true;
          this.getDetalleProveedorCategoria(this.options.idProveedorCategoria);
          this.hideStatus = true;
          this.proveedorCategoriaForm.disable();
          break;
        default:
          this.hideStatus = false
      }
    });
  }

  formProveedorCategoria(){
      this.proveedorCategoriaForm =new FormGroup({
      idProveedorCateoria: new FormControl({value: this.proveedorCategoria.idProveedorCategoria, disabled: this.hideStatus}),
      nombre: new FormControl({value: this.proveedorCategoria.nombre, disabled: this.hideStatus}, [Validators.required]),
      detalle: new FormControl({value: this.proveedorCategoria.detalle, disabled: this.hideStatus}, [Validators.required]),
      indActivo: new FormControl({value: this.proveedorCategoria.indActivo, disabled: this.hideStatus})
    });
  }
  //validation formulario
  get f() { return this.proveedorCategoriaForm.controls; }

  validInput(nameTxt){
    switch(nameTxt){
      case "nombre":
        this.statusValidateNombre = true;
        break;
      case "detalle":
        this.statusValidateDetalle = true;
        break;
    }
  }


  btnRegistrar() {
    if(!this.proveedorCategoriaForm.invalid){
      this.commonsService.openModalLoading();
      this.proveedorCategoriaService.postPutOperation(this.proveedorCategoria, this.operation)
        .pipe(
        catchError(error => {
          this.commonsService.openModalErrorServer();
          throw error;
        }),
        finalize(() => {this.commonsService.closeModalLoading()}))
        .subscribe((data) => {
          const response: GenericResponse<any> = data.body;
          switch(response.code){
            case 200:
              this.response = true;
              this.bsModalRef.hide();
              break;
            case 1:
              this.response = false;
              this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_OHOH_MODAL, response.message);
              break;
            default:
              this.response = false;
              this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
          }
        });
    }
  }

  getDetalleProveedorCategoria(idProveedorCategoria){
    this.commonsService.openModalLoading();
    this.proveedorCategoriaService.getProveedorCategoria(idProveedorCategoria)
      .pipe(
      catchError(error => {
        this.commonsService.openModalErrorServer();
        throw error;
      }),
      finalize(() => {this.commonsService.closeModalLoading()}))
      .subscribe((data) => {
        this.proveedorCategoria = data.body.body;
        this.formProveedorCategoria();
      });
  }

}