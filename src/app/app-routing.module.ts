import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

export const appRoutes: Routes = [
  { path: 'modulo-cliente',
    loadChildren: () => import('./modulo-cliente/modulo-cliente.module').then(m => m.ModuloClienteModule)
  },
  { path: 'modulo-compra/operacion',
    loadChildren: () => import('./modulo-compra/modulo-compra.module').then(m => m.ModuloCompraModule)
  },
  { path: 'modulo-venta/operacion',
    loadChildren: () => import('./modulo-venta/modulo-venta.module').then(m => m.ModuloVentaModule)
  },
  { path: 'modulo-producto/operacion',
    loadChildren: () => import('./modulo-producto/modulo-producto.module').then(m => m.ModuloProductoModule)
  },
  { path: 'modulo-proveedor/operacion',
    loadChildren: () => import('./modulo-proveedor/modulo-proveedor.module').then(m => m.ModuloProveedorModule)
  },
  { path: 'modulo-stock',
    loadChildren: () => import('./modulo-stock/modulo-stock.module').then(m => m.ModuloStockModule)
  },
  { path: 'modulo-config/configiracion',
    loadChildren: () => import('./modulo-config/modulo-config.module').then(m => m.ModuloConfigModule)
  }
];

export const SigsvAppRoute: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true, onSameUrlNavigation: 'reload' });
