import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//modulo routing
import { SigsvAppRoute } from './app-routing.module';
//modulos ngx
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxSelectModule } from 'ngx-select-ex';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgSelectModule, NgOption} from '@ng-select/ng-select';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

//modulos app
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//modulos dev
import { CommonsModule } from './commons/commons.module';
//modulos dev multi modulo
import { ModalDetailClienteComponent } from './modulo-cliente/cliente/detail/modal-detail-cliente.component';
import { ModalDetailProveedorCategoriaComponent } from './modulo-proveedor/proveedor-categoria/detail/modal-detail-proveedor-categoria.component';
import { ModalDetailProductoCategoriaComponent } from './modulo-producto/producto-categoria/detail/modal-detail-producto-categoria.component';
import { ModalDetailProveedorComponent } from './modulo-proveedor/proveedor/detail/modal-detail-proveedor.component';

//servicios dev
import { CommonsService } from './services/commons/commons.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { ProveedorCategoriaService } from 'src/app/services/proveedor/proveedor-categoria.service';
import { ProductoCategoriaService } from 'src/app/services/producto/producto-categoria.service';
import { ProveedorService } from 'src/app/services/proveedor/proveedor.service';
import { SunatReniecService } from 'src/app/services/sunat-reniec/sunat-reniec.service';
import { BsModalService } from 'ngx-bootstrap/modal';

//componentes app
import { AppComponent } from './app.component';
//componente dev
import { HeaderComponent } from './layouts/header/header.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { MenuComponent } from './layouts/menu/menu.component';
import { BreadcumbComponent } from './layouts/breadcumb/breadcumb.component';
import { MainComponent } from './layouts/main/main.component';
import { PaginationComponent } from './commons/pagination/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalDetailClienteComponent,
    ModalDetailProveedorCategoriaComponent,
    ModalDetailProductoCategoriaComponent,
    ModalDetailProveedorComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    MainComponent,
    BreadcumbComponent
  ],
  imports: [
    BrowserModule,
    SigsvAppRoute,
    CommonsModule,
    HttpClientModule,
    NgxSpinnerModule,
    NgxSelectModule,
    NgSelectModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    TooltipModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ClienteService,
    ProveedorCategoriaService,
    ProductoCategoriaService,
    ProveedorService,
    SunatReniecService,
    CommonsService,
    BsModalService
  ],
  entryComponents: [
    PaginationComponent,
    ModalDetailClienteComponent,
    ModalDetailProveedorCategoriaComponent,
    ModalDetailProductoCategoriaComponent,
    ModalDetailProveedorComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
