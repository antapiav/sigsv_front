declare var APP_CONTEXT_PATH;
declare var APP_RESOURCES_PATH;
declare var API_CONTEXT_PATH;
declare var APP_USUARIO;

export const APP_URL_LOGOUT = APP_CONTEXT_PATH + 'logoutApp';

export class AppConstants {

    public static APP_USUARIO = APP_USUARIO;

    public static APP_PAGINATION_DEFAULT_PAGE = 1;
    public static APP_PAGINATION_DEFAULT_LIMIT = 10;

    public static ID_TABLE_LST_CLIENTE = 'idTableLstCliente';
    public static ID_TABLE_LST_PROVEEDOR = 'idTableLstProveedor';
    public static ID_TABLE_LST_PROVEEDOR_CATEGORIA = 'idTableLstProveedoCategoriar';
    public static ID_TABLE_LST_PRODUCTO = 'idTableLstProducto';
    public static ID_TABLE_LST_PRODUCTO_CATEGORIA = 'idTableLstProductoCategoria';
    public static ID_TABLE_LST_USUARIO = 'idTableLstUsuario';

    public static ApiUrls = class {
        
        public static SLASH = "/";

        public static USUARIO_LST = API_CONTEXT_PATH + "usuario/pagination/list";

        public static CLIENTE_LST_PAGINADO = API_CONTEXT_PATH + "cliente/paginado?page=";
        public static CLIENTE_LST_PAGINADO2 ="&size=";
        public static CLIENTE_LST_PAGINADO_SORT = "&sort=";
        public static CLIENTE_LST_PAGINADO_NAME = ",asc&name=";
        public static CLIENTE_LST_PAGINADO5_OPERATOR = "&operator=";
        public static CLIENTE_LST_PAGINADO6_VALUE = "&value=";

        public static PROVEEDOR_CATEGORIA_LST_PAGINADO = API_CONTEXT_PATH + "proveedor_categoria/paginado?page=";
        public static PROVEEDOR_CATEGORIA_LST_PAGINADO2 ="&size=";
        public static PROVEEDOR_CATEGORIA_LST_PAGINADO_SORT = "&sort=";
        public static PROVEEDOR_CATEGORIA_LST_PAGINADO_NAME = ",asc&name=";
        public static PROVEEDOR_CATEGORIA_LST_PAGINADO5_OPERATOR = "&operator=";
        public static PROVEEDOR_CATEGORIA_LST_PAGINADO6_VALUE = "&value=";

        public static PRODUCTO_CATEGORIA_LST_PAGINADO = API_CONTEXT_PATH + "producto_categoria/paginado?page=";
        public static PRODUCTO_CATEGORIA_LST_PAGINADO2 ="&size=";
        public static PRODUCTO_CATEGORIA_LST_PAGINADO_SORT = "&sort=";
        public static PRODUCTO_CATEGORIA_LST_PAGINADO_NAME = ",asc&name=";
        public static PRODUCTO_CATEGORIA_LST_PAGINADO5_OPERATOR = "&operator=";
        public static PRODUCTO_CATEGORIA_LST_PAGINADO6_VALUE = "&value=";

        public static PROVEEDOR_LST_PAGINADO = API_CONTEXT_PATH + "proveedor/paginado?page=";
        public static PROVEEDOR_LST_PAGINADO2 ="&size=";
        public static PROVEEDOR_LST_PAGINADO_SORT = "&sort=";
        public static PROVEEDOR_LST_PAGINADO_NAME = ",asc&name=";
        public static PROVEEDOR_LST_PAGINADO5_OPERATOR = "&operator=";
        public static PROVEEDOR_LST_PAGINADO6_VALUE = "&value=";
        public static PROVEEDOR_LST_PAGINADO7_ID_PROVEEDOR_CATEGORIA = "&idProveedorCategoria=";

        public static PRODUCTO_LST_PAGINADO = API_CONTEXT_PATH + "producto/paginado?page=";
        public static PRODUCTO_LST_PAGINADO2 ="&size=";
        public static PRODUCTO_LST_PAGINADO_SORT = "&sort=";
        public static PRODUCTO_LST_PAGINADO_NAME = ",asc&name=";
        public static PRODUCTO_LST_PAGINADO5_OPERATOR = "&operator=";
        public static PRODUCTO_LST_PAGINADO6_VALUE = "&value=";

        public static CLIENTE_INSERTAR = API_CONTEXT_PATH + "cliente/insertar";
        public static CLIENTE_MODIFICAR = API_CONTEXT_PATH + "cliente/modificar";
        public static CLIENTE_GET_CLIENTE = API_CONTEXT_PATH + "cliente/detalle/";
        public static CLIENTE_IND_ACTIVO = API_CONTEXT_PATH + "cliente/ind_activo/";

        public static PROVEEDOR_INSERTAR = API_CONTEXT_PATH + "proveedor/insertar";
        public static PROVEEDOR_MODIFICAR = API_CONTEXT_PATH + "proveedor/modificar";
        public static PROVEEDOR_GET_PROVEEDOR = API_CONTEXT_PATH + "proveedor/detalle/";
        public static PROVEEDOR_IND_ACTIVO = API_CONTEXT_PATH + "proveedor/ind_activo/";

        public static PROVEEDOR_CATEGORIA_INSERTAR = API_CONTEXT_PATH + "proveedor_categoria/insertar";
        public static PROVEEDOR_CATEGORIA_MODIFICAR = API_CONTEXT_PATH + "proveedor_categoria/modificar";
        public static PROVEEDOR_CATEGORIA_GET_PROVEEDOR_CATEGORIA = API_CONTEXT_PATH + "proveedor_categoria/detalle/";
        public static PROVEEDOR_CATEGORIA_IND_ACTIVO = API_CONTEXT_PATH + "proveedor_categoria/ind_activo/";
        public static PROVEEDOR_CATEGORIA_GET_LST = API_CONTEXT_PATH + "proveedor_categoria/lst?value=";

        public static PRODUCTO_INSERTAR = API_CONTEXT_PATH + "producto/insertar";
        public static PRODUCTO_MODIFICAR = API_CONTEXT_PATH + "producto/modificar";
        public static PRODUCTO_GET_PRODUCTO = API_CONTEXT_PATH + "producto/detalle/";
        public static PRODUCTO_IND_ACTIVO = API_CONTEXT_PATH + "producto/ind_activo/";

        public static PRODUCTO_CATEGORIA_INSERTAR = API_CONTEXT_PATH + "producto_categoria/insertar";
        public static PRODUCTO_CATEGORIA_MODIFICAR = API_CONTEXT_PATH + "producto_categoria/modificar";
        public static PRODUCTO_CATEGORIA_GET_PRODUCTO_CATEGORIA = API_CONTEXT_PATH + "producto_categoria/detalle/";
        public static PRODUCTO_CATEGORIA_IND_ACTIVO = API_CONTEXT_PATH + "producto_categoria/ind_activo/";
        public static PRODUCTO_CATEGORIA_GET_LST = API_CONTEXT_PATH + "producto_categoria/lst/";

        public static CONSULTA_SUNAT_DNI = API_CONTEXT_PATH + "consulta/sunat/";
        public static CONSULTA_RENIEC_RUC = API_CONTEXT_PATH +"consulta/reniec/" ;
    }

    public static Field = class {
        public static STRING_VACIO = '';
        public static DEFAULT_SEARCH = "--Seleccionar--";

        public static STRING_CARGANDO = "Cargando...";
        public static STRING_ERROR_CARGA = "Error al cargar :(";
        public static STRING_SIN_RESULTADOS = "Sin resultados :(";
        public static STRING_SELECCIONE_CATEGORIA = "Seleccione categoria";


        public static CLIENTE_ID_NAME = "idCliente";
        public static PROVEEDOR_CATEGORIA_ID_NAME = "idProveedorCategoria";
        public static PRODUCTO_CATEGORIA_ID_NAME = "idProductoCategoria";
        public static PROVEEDOR_ID_NAME = "idProveedor";
        public static PRODUCTO_ID_NAME = "idProducto";

        public static STRING_NOMBRE = 'nombre';
        public static STRING_CATEGORIA = 'categoria';
        public static STRING_DNI = 'dni';
        public static STRING_CODIGO = 'codigo';

        public static STRING_SUNAT = 'SUNAT';
        public static STRING_RENIEC = 'RENIEC';

        public static INT_SIZE_TXT_DNI = 8;
        public static INT_SIZE_TXT_RUC = 11;
    }

    public static Modal = class {
        public static TITLE_MODAL_REGISTRAR_CLIENTE = "Registrar Cliente";
        public static TITLE_MODAL_ACTUALIZAR_CLIENTE = "Actualizar Cliente";
        public static TITLE_MODAL_DETALLE_CLIENTE = "Detalle Cliente";

        public static TITLE_MODAL_REGISTRAR_PROVEEDOR = "Registrar Proveedor";
        public static TITLE_MODAL_ACTUALIZAR_PROVEEDOR = "Actualizar Proveedor";
        public static TITLE_MODAL_DETALLE_PROVEEDOR = "Detalle Proveedor";

        public static TITLE_MODAL_REGISTRAR_PROVEEDOR_CATEGORIA = "Registrar Categoria Proveedor";
        public static TITLE_MODAL_ACTUALIZAR_PROVEEDOR_CATEGORIA = "Actualizar Categoria Proveedor";
        public static TITLE_MODAL_DETALLE_PROVEEDOR_CATEGORIA = "Detalle Categoria Proveedor";

        public static TITLE_MODAL_REGISTRAR_PRODUCTO = "Registrar Producto";
        public static TITLE_MODAL_ACTUALIZAR_PRODUCTO = "Actualizar Producto";
        public static TITLE_MODAL_DETALLE_PRODUCTO = "Detalle Producto";

        public static TITLE_MODAL_REGISTRAR_PRODUCTO_CATEGORIA = "Registrar Categoria Producto";
        public static TITLE_MODAL_ACTUALIZAR_PRODUCTO_CATEGORIA = "Actualizar Categoria Producto";
        public static TITLE_MODAL_DETALLE_PRODUCTO_CATEGORIA = "Detalle Categoria Producto";

        public static OP_SAVE = "save";
        public static OP_UPDATE = "update";
        public static OP_DETAIL = "detail";
        public static BTN_REGISTRAR = "Registrar";
        public static BTN_ACTUALIZAR = "Actualizar";

    }

}
