import { Renderer2, ElementRef } from "@angular/core";
import { DecimalPipe } from "@angular/common";

import { AppConstants } from "./app.constants";

export class AppUtils {

    public static decimalPipe: DecimalPipe = new DecimalPipe("es-PE");

    public static onElementFocus(id: string, element: ElementRef): void {
        const elementFocus = element.nativeElement.querySelector('#' + id);
        if (elementFocus) {
            setTimeout(() => {
                elementFocus.focus();
                elementFocus.scrollIntoView(true);
            }, 0);
        }
    }

    public static concatCriterioIn(value: any): string {
        let valueIn = value as Array<any>;
        valueIn = valueIn.map((item) => item instanceof Array ? item.join('|') : item);
        return valueIn.join('|');
    }

}
