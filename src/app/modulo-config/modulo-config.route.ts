import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { usuarioRoute } from './usuario/usuario.route';

const MODULO_CONFIG_ROUTES = [
    usuarioRoute
];

export const modulouConfigRoutes: Routes = MODULO_CONFIG_ROUTES;

export const SigsvModuloConfigRoutes: ModuleWithProviders = RouterModule.forChild(modulouConfigRoutes);