import { Route } from '@angular/router';

import { UsuarioComponent } from './usuario.component';

export const usuarioRoute: Route = {
    path: 'usuario', component: UsuarioComponent,
    data: {
        title: 'Modulo Configuraciones - Usuarios',
        breadcumb: ['SIGSV', 'Configuraciones', 'Usuarios', 'Operaciones']
    }
};