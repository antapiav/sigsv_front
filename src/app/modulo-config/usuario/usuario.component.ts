import { Component, OnInit, ElementRef } from '@angular/core';
import Swal from 'sweetalert2/src/sweetalert2.js';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  openModalSweet(){
    //Swal('Registro exitoso...', this.titularAlerta, 'success');
    Swal.fire({
      title: 'Error!',
      text: 'Do you want to continue',
      icon: 'error',
      confirmButtonText: 'Cool'
    });
  }

}
