import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { SigsvModuloConfigRoutes } from './modulo-config.route';

import { UsuarioComponent } from './usuario/usuario.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        ModalModule,
        CollapseModule,
        BsDropdownModule,

        SigsvModuloConfigRoutes
    ],
    providers: [],
    declarations: [
        UsuarioComponent
    ]
  })
  export class ModuloConfigModule { }