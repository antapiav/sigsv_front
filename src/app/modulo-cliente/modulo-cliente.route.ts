import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { clienteRoute } from './cliente/cliente.route';

const MODULO_CLIENTE_ROUTES = [
    clienteRoute
];

export const moduloClienteRoutes: Routes = MODULO_CLIENTE_ROUTES;

export const SigsvModuloClienteRoutes: ModuleWithProviders = RouterModule.forChild(moduloClienteRoutes);