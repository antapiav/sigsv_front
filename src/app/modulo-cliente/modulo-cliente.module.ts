import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//modulo ngx
import { AccordionModule } from 'ngx-bootstrap/accordion';
//module dev
import { CommonsModule } from '../commons/commons.module';
//modulo routing
import { SigsvModuloClienteRoutes } from './modulo-cliente.route';
//servicios ngx
//servicios dev
//component inerno
import { ClienteComponent } from './cliente/cliente.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CommonsModule,

        AccordionModule,

        SigsvModuloClienteRoutes
    ],
    providers: [
    ],
    declarations: [
        ClienteComponent
    ]
  })
  export class ModuloClienteModule { }