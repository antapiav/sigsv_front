import { Route } from '@angular/router';

import { ClienteComponent } from './cliente.component';

export const clienteRoute: Route = {
    path: 'cliente', component: ClienteComponent,
    data: {
        title: 'Módulo Clientes',
        breadcumb: ['SIGSV', 'Clientes', 'Operaciones']
    }
};
