import { Component, OnInit, Renderer2, HostListener, Output, EventEmitter, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { Cliente } from 'src/app/model/cliente.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AppConstants } from 'src/app/util/app.constants';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { ClienteService } from 'src/app/services/cliente/cliente.service';
import { SunatReniecService } from 'src/app/services/sunat-reniec/sunat-reniec.service';
import { catchError, finalize, timeout } from 'rxjs/operators';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { AppMessages } from 'src/app/services/app.messages';

@Component({
  selector: 'app-modal-detail-cliente',
  templateUrl: './modal-detail-cliente.component.html',
  styleUrls: ['./modal-detail-cliente.component.css']
})
export class ModalDetailClienteComponent implements OnInit {
  //variables entrada open modal
  public titulo: string;
  public operation: string;
  public options: any;
  //varaibles modal front
  public disable: boolean = false;
  public disableBtnRegistrar: boolean = false;
  public hideStatus: boolean = false;
  public nameBtnOperation: string;
  //variables propias
  public cliente: Cliente;
  public response: boolean = false;
  //varaibles form validator
  public clienteForm: FormGroup;
  public statusValidateDni = false;
  public statusValidateNombre = false;
  public statusValidateApPaterno = false;
  public statusValidateApMaterno = false;
  public statusValidateEmail = false;


  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private clienteService: ClienteService,
    private sunatReniecService: SunatReniecService,
    private commonsService: CommonsService
  ) {
    this.cliente = new Cliente();
  }

  ngOnInit() {
    this.formCliente();
    setTimeout(() => {
      switch(this.operation){
        case AppConstants.Modal.OP_SAVE:
          this.hideStatus = false;
          this.disableBtnRegistrar = true;
          this.cliente.indActivo = true;
          this.nameBtnOperation = AppConstants.Modal.BTN_REGISTRAR;
          this.clienteForm.controls['indActivo'].disable();
          break;
        case AppConstants.Modal.OP_UPDATE:
          this.getDetalleCliente(this.options.idCliente);
          this.hideStatus = false;
          this.disableBtnRegistrar = false;
          this.nameBtnOperation = AppConstants.Modal.BTN_ACTUALIZAR;
          break;
        case AppConstants.Modal.OP_DETAIL:
          this.disable = true;
          this.disableBtnRegistrar = true;
          this.getDetalleCliente(this.options.idCliente);
          this.hideStatus = true;
          this.clienteForm.disable();
          break;
        default:
          this.hideStatus = false
      }
    });
  }

  formCliente(){
      this.clienteForm =new FormGroup({
      idCliente: new FormControl({value: this.cliente.idCliente, disabled: this.hideStatus}),
      nombre: new FormControl({value: this.cliente.nombre, disabled: this.hideStatus}, [Validators.required]),
      apPaterno: new FormControl({value: this.cliente.apPaterno, disabled: this.hideStatus}, [Validators.required]),
      apMaterno: new FormControl({value: this.cliente.apMaterno, disabled: this.hideStatus}, [Validators.required]),
      dni: new FormControl({value: this.cliente.dni, disabled: this.hideStatus}, [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      direccion: new FormControl({value: this.cliente.direccion, disabled: this.hideStatus}),
      contacto: new FormControl({value: this.cliente.contacto, disabled: this.hideStatus}),
      telefono: new FormControl({value: this.cliente.telefono, disabled: this.hideStatus}),
      email: new FormControl({value: this.cliente.email, disabled: this.hideStatus}, [Validators.email]),
      indActivo: new FormControl({value: this.cliente.indActivo, disabled: this.hideStatus})
    });
  }
  //validation formulario
  get f() { return this.clienteForm.controls; }

  validInput(nameTxt){
    switch(nameTxt){
      case "dni":
        this.statusValidateDni = true;
        break;
      case "nombre":
        this.statusValidateNombre = true;
        break;
      case "apPaterno":
        this.statusValidateApPaterno = true;
        break;
      case "apMaterno":
        this.statusValidateApMaterno = true;
        break;
      case "email":
        this.statusValidateEmail = true;
        break;
    }
  }


  btnRegistrarCliente() {
    if(!this.clienteForm.invalid){
      this.commonsService.openModalLoading();
      this.clienteService.postPutOperation(this.cliente, this.operation)
        .pipe(
        catchError(error => {
          this.commonsService.openModalErrorServer();
          throw error;
        }),
        finalize(() => {this.commonsService.closeModalLoading()}))
        .subscribe((data) => {
          const response: GenericResponse<any> = data.body;
          switch(response.code){
            case 200:
              this.response = true;
              this.bsModalRef.hide();
              break;
            case 1:
              this.response = false;
              this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_OHOH_MODAL, response.message);
              break;
            default:
              this.response = false;
              this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
          }
        });
    }
  }

  getDataRenic(){
    this.commonsService.openModalLoading();
    this.sunatReniecService.getDataReniec(this.cliente.dni)
    .pipe(
      timeout(60000),
      catchError(error => {
        this.commonsService.openModalErrorServer();
        throw error;
      }),
      finalize(() => {this.commonsService.closeModalLoading()}))
      .subscribe((data) => {
        if(data.body.code!=0){
          this.cliente.dni = data.body.body.dni;
          this.cliente.nombre = data.body.body.nombre;
          this.cliente.apPaterno = data.body.body.apPaterno;
          this.cliente.apMaterno = data.body.body.apMaterno;
        }else{
          this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_SEGURO_MODAL, data.body.message);
        }
      });
  }

  getDetalleCliente(idCliente){
    this.commonsService.openModalLoading();
    this.clienteService.getCliente(idCliente)
      .pipe(
      catchError(error => {
        this.commonsService.openModalErrorServer();
        throw error;
      }),
      finalize(() => {this.commonsService.closeModalLoading()}))
      .subscribe((data) => {
        this.cliente = data.body.body;
        this.formCliente();
      });
  }

}