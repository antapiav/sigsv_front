import { Component, OnInit, TemplateRef, ElementRef } from '@angular/core';
//componentes angular
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
//servicios dev
import { ClienteService } from 'src/app/services/cliente/cliente.service';
//servicio app
import { catchError, finalize } from 'rxjs/operators';
//model
import { Cliente } from 'src/app/model/cliente.model';
import { AppConstants } from 'src/app/util/app.constants';
import { Pagination } from 'src/app/model/pagination.model';
import { GenericSearch } from 'src/app/model/search/generic-search.model';
//modules internos
import { ModalDetailClienteComponent } from './detail/modal-detail-cliente.component';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { AppMessages } from 'src/app/services/app.messages';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {  
  //variables select
  selectCliente = [
    {id: 'nombre', text: 'Nombre'},
    {id: 'dni', text: 'D.N.I.'}
  ]
  //variable acordion
  estadoAcordion = true;
  //variables beans model
  public searchCliente: GenericSearch;
  public paginationCliente: Pagination;
  public lstCliente: Array<Cliente>;
  //variables paginacion
  public headPage: any;
  //variables ngx modal
  public modalRef: BsModalRef;
  //variables otras
  //public defaultTipoSearch: string;
  //public stringVacio: string;//para html

  constructor(
    private clienteService: ClienteService,
    private element: ElementRef,
    private modalService: BsModalService,
    private commonsService: CommonsService
    ) { 
      this.searchCliente = new GenericSearch();
      this.paginationCliente = new Pagination();
      this.paginationCliente.createFilters();
      this.lstCliente = new Array();
      //this.defaultTipoSearch = AppConstants.Field.DEFAULT_SEARCH;
      //this.stringVacio = AppConstants.Field.STRING_VACIO;
    }

  ngOnInit(): void {
    this.searchCliente.tipo = AppConstants.Field.STRING_NOMBRE;
    this.paginationCliente.clearFilters();
    this.reqLstClientePaginado(this.paginationCliente.page);
  }
  //operaciones vistas
  btnBuscarLstCliente(){
    this.paginationCliente.applyFilterEquals(this.searchCliente.tipo, this.searchCliente.dato);
    this.reqLstClientePaginado();
  }

  btnLimpiar(){
    this.searchCliente.tipo = AppConstants.Field.STRING_NOMBRE;
    this.searchCliente.dato = AppConstants.Field.STRING_VACIO;
  }

  changeSlxTipoDocumento(event: string){
    this.paginationCliente.clearFilters();
  }

  btnNuevoCliente(){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_REGISTRAR_CLIENTE,
      operation: AppConstants.Modal.OP_SAVE,
    };
    this.commonsService.openModalLarge(
      ModalDetailClienteComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.paginationCliente.clearFilters();
          this.reqLstClientePaginado(this.paginationCliente.page);
        }
      });
  }

  btnDescargarExcel(){}

  btnRefreshLstCliente(){
    this.paginationCliente.clearFilters();
    this.reqLstClientePaginado(this.paginationCliente.page);
  }

  btnDetalleCliente(cliente: Cliente){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_DETALLE_CLIENTE,
      operation: AppConstants.Modal.OP_DETAIL,
      options: cliente
    };
    this.commonsService.openModalLarge(
      ModalDetailClienteComponent,
      params)
      .subscribe((response) => {
      });
  }

  btnEditarCliente(cliente: Cliente){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_ACTUALIZAR_CLIENTE,
      operation: AppConstants.Modal.OP_UPDATE,
      options: cliente
    };
    this.commonsService.openModalLarge(
      ModalDetailClienteComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.paginationCliente.clearFilters();
          this.reqLstClientePaginado(this.paginationCliente.page);
        }
      });
  }

  async btnBajaCliente(cliente: Cliente){
    const  a = await this.commonsService.openModalConfirmQuestion();
    if(a.isConfirmed){
      this.commonsService.openModalLoading();
    this.clienteService.indActivoCliente(cliente.idCliente)
        .pipe(
          catchError(error => {
            this.commonsService.openModalErrorServer();
            throw error;
          }),
          finalize(() => {this.commonsService.closeModalLoading()}))
        .subscribe((data) => {
            const response: GenericResponse<any> = data.body;
            switch(response.code){
              case 200:
                this.reqLstClientePaginado();
                this.commonsService.openModalExitoOperation();
                break;
              default:
                this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
            }
        });
    }
  }

  //operaciones request
  //--------------------------------------------
  reqLstClientePaginado(page?: number) {  
    this.paginationCliente.showLoading();
    this.paginationCliente.onPage(page);
    this.clienteService.getAll(this.paginationCliente, this.searchCliente)
    .pipe(
      catchError(error => {
        console.log('error occured:', error);
        throw error;
      }),
      finalize(() => {
        this.paginationCliente.onFocus(AppConstants.ID_TABLE_LST_CLIENTE, this.element);
        this.paginationCliente.hideLoading();
      })
    ).subscribe((data) => {
        this.lstCliente = data.body.content;
        const total = data.body.totalElements;
        const pagina = (data.body.number)+1;
        this.headPage = {pagina, total};
        this.paginationCliente.setConfig(this.headPage);
      });
  }
}
