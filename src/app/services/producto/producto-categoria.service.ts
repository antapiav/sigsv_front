import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

import { AppConstants } from '../../util/app.constants';
import { Pagination } from '../../model/pagination.model';
import { GenericResponse } from '../../model/generic-response.model';
import { Observable } from 'rxjs';
import { PageableResponse } from 'src/app/model/pageable-response.model';
import { ProductoCategoria } from 'src/app/model/producto-categoria.model';

@Injectable()
export class ProductoCategoriaService {

  constructor(private http: HttpClient) { }

  getAll(pagination: Pagination): Observable<HttpResponse<PageableResponse<Array<ProductoCategoria>>>> {
    const page= (pagination.page)-1;
    const limit= pagination.limit;

    return this.http.get<PageableResponse<Array<ProductoCategoria>>>(
      `${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_LST_PAGINADO}`+
      `${page}`+
      `${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_LST_PAGINADO2}`+
      `${limit}`+
      `${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_LST_PAGINADO_SORT}`+
      AppConstants.Field.PRODUCTO_CATEGORIA_ID_NAME+
      `${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_LST_PAGINADO_NAME}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].name}`+
      `${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_LST_PAGINADO5_OPERATOR}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].operator}`+
      `${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_LST_PAGINADO6_VALUE}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].value}`, 
      {
      observe: 'response'
    });
  }

  getProductoCategoria(idProductoCategoria): Observable<HttpResponse<GenericResponse<ProductoCategoria>>> {
    return this.http.get<GenericResponse<ProductoCategoria>>(`${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_GET_PRODUCTO_CATEGORIA}${idProductoCategoria}`, {
      observe: 'response'
    });
  }

  indActivoProductoCategoria(idProductoCategoria): Observable<HttpResponse<GenericResponse<Array<ProductoCategoria>>>> {
    return this.http.get<GenericResponse<Array<ProductoCategoria>>>(`${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_IND_ACTIVO}${idProductoCategoria}`, {
      observe: 'response'
    });
  }

  postPutOperation(productoCategoria: ProductoCategoria, operation: string): Observable<HttpResponse<GenericResponse<ProductoCategoria>>>{
    if(operation===AppConstants.Modal.OP_SAVE){
      return this.postProductoCategoria(productoCategoria);
    }else if(operation===AppConstants.Modal.OP_UPDATE){
      return this.putProductoCategoria(productoCategoria);
    }
  }

  postProductoCategoria(productoCategoria: ProductoCategoria): Observable<HttpResponse<GenericResponse<ProductoCategoria>>> {
    return this.http.post<GenericResponse<ProductoCategoria>>(`${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_INSERTAR}`, productoCategoria,{
      observe: 'response'
    });
  }

  putProductoCategoria(productoCategoria: ProductoCategoria): Observable<HttpResponse<GenericResponse<ProductoCategoria>>> {
    return this.http.put<GenericResponse<ProductoCategoria>>(`${AppConstants.ApiUrls.PRODUCTO_CATEGORIA_MODIFICAR}`, productoCategoria,{
      observe: 'response'
    });
  }

}