import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { CommonsService } from './commons/commons.service';
import { BrowserModule } from '@angular/platform-browser';
//import { AssetsLocationPipe } from './assets-location.pipe';
//import { SplitPipe } from './split.pipe';

@NgModule({
  imports: [
  ],
  providers: [
    //CommonsService
  ],
  exports: [
    //SplitPipe,
    //AssetsLocationPipe
  ],
  declarations: [
    //SplitPipe,
    //AssetsLocationPipe
  ]
})
export class ServicesModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ServicesModule,
      providers: [
        //CommonsService
      ]
    };
  }
}
