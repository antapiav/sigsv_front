export class AppMessages {
    public static TITLE_ALERT = 'Alerta';

    public static NUM_BIEN_EXISTE = 'Numero de bien ya existe...';

    public static EXPIRE_SESSION_TIME_OUT = 'El tiempo de su sesión ha expirado';

    public static GENERIC_MSG_ERROR_MODAL = 'Ocurrio un error en la operación :(';

    public static GENERIC_TITLE_SEGURO_MODAL = 'Seguro?';
    public static GENERIC_TITLE_OHOH_MODAL = 'OhOh...';
    public static NO_EXISTE_DOCUMENTO = 'No se encontro registro con ese número...';


}
