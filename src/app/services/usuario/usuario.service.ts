import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

import { AppConstants } from '../../util/app.constants';
import { Pagination } from '../../model/pagination.model';
import { GenericResponse } from '../../model/generic-response.model';
import { Cliente } from '../../model/cliente.model';
import { Observable } from 'rxjs';


@Injectable()
export class UsuarioService {

  constructor(private http: HttpClient) { }

  getAll(pagination: Pagination): Observable<HttpResponse<GenericResponse<Array<Cliente>>>> {
    const paginationRequest = {
      page: pagination.page,
      limit: pagination.limit,
      filters: pagination.joinFilters()
    };

    return this.http.post<GenericResponse<Array<Cliente>>>(`${AppConstants.ApiUrls.USUARIO_LST}`, paginationRequest, {
        observe: 'response'
      });
  }

}