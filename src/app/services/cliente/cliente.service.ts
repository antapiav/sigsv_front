import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

import { AppConstants } from '../../util/app.constants';
import { Pagination } from '../../model/pagination.model';
import { GenericResponse } from '../../model/generic-response.model';
import { Cliente } from '../../model/cliente.model';
import { Observable } from 'rxjs';
import { PageableResponse } from 'src/app/model/pageable-response.model';

@Injectable()
export class ClienteService {

  constructor(private http: HttpClient) { }

  getAll(pagination: Pagination, searchCliente): Observable<HttpResponse<PageableResponse<Array<Cliente>>>> {
    const page= (pagination.page)-1;
    const limit= pagination.limit;

    return this.http.get<PageableResponse<Array<Cliente>>>(
      `${AppConstants.ApiUrls.CLIENTE_LST_PAGINADO}`+
      `${page}`+
      `${AppConstants.ApiUrls.CLIENTE_LST_PAGINADO2}`+
      `${limit}`+
      `${AppConstants.ApiUrls.CLIENTE_LST_PAGINADO_SORT}`+
      AppConstants.Field.CLIENTE_ID_NAME+
      `${AppConstants.ApiUrls.CLIENTE_LST_PAGINADO_NAME}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].name}`+
      `${AppConstants.ApiUrls.CLIENTE_LST_PAGINADO5_OPERATOR}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].operator}`+
      `${AppConstants.ApiUrls.CLIENTE_LST_PAGINADO6_VALUE}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].value}`, 
      {
      observe: 'response'
    });
  }

  getCliente(idCliente): Observable<HttpResponse<GenericResponse<Cliente>>> {
    return this.http.get<GenericResponse<Cliente>>(`${AppConstants.ApiUrls.CLIENTE_GET_CLIENTE}${idCliente}`, {
      observe: 'response'
    });
  }

  indActivoCliente(idCliente): Observable<HttpResponse<GenericResponse<Array<Cliente>>>> {
    return this.http.get<GenericResponse<Array<Cliente>>>(`${AppConstants.ApiUrls.CLIENTE_IND_ACTIVO}${idCliente}`, {
      observe: 'response'
    });
  }

  postPutOperation(cliente: Cliente, operation: string): Observable<HttpResponse<GenericResponse<Cliente>>>{
    if(operation===AppConstants.Modal.OP_SAVE){
      return this.postClietne(cliente);
    }else if(operation===AppConstants.Modal.OP_UPDATE){
      return this.putCliente(cliente);
    }
  }

  postClietne(cliente: Cliente): Observable<HttpResponse<GenericResponse<Cliente>>> {
    return this.http.post<GenericResponse<Cliente>>(`${AppConstants.ApiUrls.CLIENTE_INSERTAR}`, cliente,{
      observe: 'response'
    });
  }

  putCliente(cliente: Cliente): Observable<HttpResponse<GenericResponse<Cliente>>> {
    return this.http.put<GenericResponse<Cliente>>(`${AppConstants.ApiUrls.CLIENTE_MODIFICAR}`, cliente,{
      observe: 'response'
    });
  }

}