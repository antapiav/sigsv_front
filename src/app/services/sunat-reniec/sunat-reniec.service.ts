import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

import { AppConstants } from '../../util/app.constants';
import { GenericResponse } from '../../model/generic-response.model';
import { Observable } from 'rxjs';
import { Reniec } from 'src/app/model/reniec';
import { Sunat } from 'src/app/model/sunat';

@Injectable()
export class SunatReniecService {

  constructor(private http: HttpClient) { }

  getDataReniec(numDocumento): Observable<HttpResponse<GenericResponse<Reniec>>> {

    return this.http.get<GenericResponse<Reniec>>(`${AppConstants.ApiUrls.CONSULTA_RENIEC_RUC}${numDocumento}`, {
      observe: 'response'
    });
  }

  getDataSunat(numDocumento): Observable<HttpResponse<GenericResponse<Sunat>>> {

    return this.http.get<GenericResponse<Sunat>>(`${AppConstants.ApiUrls.CONSULTA_SUNAT_DNI}${numDocumento}`, {
      observe: 'response'
    });
  }
}