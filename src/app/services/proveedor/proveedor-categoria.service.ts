import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

import { AppConstants } from '../../util/app.constants';
import { Pagination } from '../../model/pagination.model';
import { GenericResponse } from '../../model/generic-response.model';
import { Observable } from 'rxjs';
import { PageableResponse } from 'src/app/model/pageable-response.model';
import { ProveedorCategoria } from 'src/app/model/proveedor-categoria.model';

@Injectable()
export class ProveedorCategoriaService {

  constructor(private http: HttpClient) { }

  getAll(pagination: Pagination): Observable<HttpResponse<PageableResponse<Array<ProveedorCategoria>>>> {
    const page= (pagination.page)-1;
    const limit= pagination.limit;

    return this.http.get<PageableResponse<Array<ProveedorCategoria>>>(
      `${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_LST_PAGINADO}`+
      `${page}`+
      `${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_LST_PAGINADO2}`+
      `${limit}`+
      `${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_LST_PAGINADO_SORT}`+
      AppConstants.Field.PROVEEDOR_CATEGORIA_ID_NAME+
      `${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_LST_PAGINADO_NAME}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].name}`+
      `${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_LST_PAGINADO5_OPERATOR}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].operator}`+
      `${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_LST_PAGINADO6_VALUE}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].value}`, 
      {
      observe: 'response'
    });
  }

  getProveedorCategoria(idProveedorCategoria): Observable<HttpResponse<GenericResponse<ProveedorCategoria>>> {
    return this.http.get<GenericResponse<ProveedorCategoria>>(`${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_GET_PROVEEDOR_CATEGORIA}${idProveedorCategoria}`, {
      observe: 'response'
    });
  }

  getLstProveedorCategoria(value): Observable<HttpResponse<GenericResponse<Array<ProveedorCategoria>>>> {
    return this.http.get<GenericResponse<Array<ProveedorCategoria>>>(`${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_GET_LST}${value}`, {
      observe: 'response'
    });
  }

  indActivoProveedorCategoria(idProveedorCategoria): Observable<HttpResponse<GenericResponse<Array<ProveedorCategoria>>>> {
    return this.http.get<GenericResponse<Array<ProveedorCategoria>>>(`${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_IND_ACTIVO}${idProveedorCategoria}`, {
      observe: 'response'
    });
  }

  postPutOperation(proveedorCategoria: ProveedorCategoria, operation: string): Observable<HttpResponse<GenericResponse<ProveedorCategoria>>>{
    if(operation===AppConstants.Modal.OP_SAVE){
      return this.postProveedorCategoria(proveedorCategoria);
    }else if(operation===AppConstants.Modal.OP_UPDATE){
      return this.putProveedorCategoria(proveedorCategoria);
    }
  }

  postProveedorCategoria(proveedorCategoria: ProveedorCategoria): Observable<HttpResponse<GenericResponse<ProveedorCategoria>>> {
    return this.http.post<GenericResponse<ProveedorCategoria>>(`${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_INSERTAR}`, proveedorCategoria,{
      observe: 'response'
    });
  }

  putProveedorCategoria(proveedorCategoria: ProveedorCategoria): Observable<HttpResponse<GenericResponse<ProveedorCategoria>>> {
    return this.http.put<GenericResponse<ProveedorCategoria>>(`${AppConstants.ApiUrls.PROVEEDOR_CATEGORIA_MODIFICAR}`, proveedorCategoria,{
      observe: 'response'
    });
  }

}