import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';

import { AppConstants } from '../../util/app.constants';
import { Pagination } from '../../model/pagination.model';
import { GenericResponse } from '../../model/generic-response.model';
import { Cliente } from '../../model/cliente.model';
import { Observable } from 'rxjs';
import { PageableResponse } from 'src/app/model/pageable-response.model';
import { Proveedor } from 'src/app/model/proveedor.model';

@Injectable()
export class ProveedorService {

  constructor(private http: HttpClient) { }
  getAll(pagination: Pagination, idProveedorCategoria: any): Observable<HttpResponse<PageableResponse<Array<Proveedor>>>> {
    const page= (pagination.page)-1;
    const limit= pagination.limit;

    return this.http.get<PageableResponse<Array<Proveedor>>>(
      `${AppConstants.ApiUrls.PROVEEDOR_LST_PAGINADO}`+
      `${page}`+
      `${AppConstants.ApiUrls.PROVEEDOR_LST_PAGINADO2}`+
      `${limit}`+
      `${AppConstants.ApiUrls.PROVEEDOR_LST_PAGINADO_SORT}`+
      AppConstants.Field.PROVEEDOR_ID_NAME+
      `${AppConstants.ApiUrls.PROVEEDOR_LST_PAGINADO_NAME}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].name}`+
      `${AppConstants.ApiUrls.PROVEEDOR_LST_PAGINADO5_OPERATOR}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].operator}`+
      `${AppConstants.ApiUrls.PROVEEDOR_LST_PAGINADO6_VALUE}`+
      `${pagination.filters[0] == null ? AppConstants.Field.STRING_VACIO : pagination.filters[0].value}`+
      `${AppConstants.ApiUrls.PROVEEDOR_LST_PAGINADO7_ID_PROVEEDOR_CATEGORIA}${idProveedorCategoria}`,
      {
      observe: 'response'
    });
  }

  async getProveedor(idProveedor): Promise<Observable<HttpResponse<GenericResponse<Proveedor>>>> {
    return this.http.get<GenericResponse<Proveedor>>(`${AppConstants.ApiUrls.PROVEEDOR_GET_PROVEEDOR}${idProveedor}`, {
      observe: 'response'
    });
  }

  indActivoProveedor(idProveedor): Observable<HttpResponse<GenericResponse<Array<Proveedor>>>> {
    return this.http.get<GenericResponse<Array<Proveedor>>>(`${AppConstants.ApiUrls.PROVEEDOR_IND_ACTIVO}${idProveedor}`, {
      observe: 'response'
    });
  }

  postPutOperation(proveedor: Proveedor, operation: string): Observable<HttpResponse<GenericResponse<Proveedor>>>{
    if(operation===AppConstants.Modal.OP_SAVE){
      return this.postProveedor(proveedor);
    }else if(operation===AppConstants.Modal.OP_UPDATE){
      return this.putProveedor(proveedor);
    }
  }

  postProveedor(proveedor: Proveedor): Observable<HttpResponse<GenericResponse<Proveedor>>> {
    return this.http.post<GenericResponse<Proveedor>>(`${AppConstants.ApiUrls.PROVEEDOR_INSERTAR}`, proveedor,{
      observe: 'response'
    });
  }

  putProveedor(proveedor: Proveedor): Observable<HttpResponse<GenericResponse<Proveedor>>> {
    return this.http.put<GenericResponse<Proveedor>>(`${AppConstants.ApiUrls.PROVEEDOR_MODIFICAR}`, proveedor,{
      observe: 'response'
    });
  }
}