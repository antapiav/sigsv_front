import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pagination } from '../../model/pagination.model';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Input()
  public pagination: Pagination;
  @Output()
  public loadPage = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  onFirst(): void {
    this.pagination.onFirst();
    this.loadPage.emit(this.pagination.page);
  }

  onPrev(): void {
    this.pagination.onPrev();
    this.loadPage.emit(this.pagination.page);
  }

  onPage(): void {
    this.loadPage.emit(this.pagination.page);
  }

  onNext(): void {
    this.pagination.onNext();
    this.loadPage.emit(this.pagination.page);
  }

  onLast(): void {
    this.pagination.onLast();
    this.loadPage.emit(this.pagination.page);
  }

  keyUpPage(event: any): void {
    const value = event.target.value;
    if (value < 1) {
      this.pagination.page = 1;
    } else if (value > this.pagination.getTotalPages()) {
      this.pagination.page = this.pagination.getTotalPages();
    }
  }

}
