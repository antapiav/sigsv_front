import { Component, OnInit, ElementRef } from '@angular/core';
import { GenericSearch } from 'src/app/model/search/generic-search.model';
import { Pagination } from 'src/app/model/pagination.model';
import { ProductoCategoria } from 'src/app/model/producto-categoria.model';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { ProductoCategoriaService } from 'src/app/services/producto/producto-categoria.service';
import { AppConstants } from 'src/app/util/app.constants';
import { catchError, finalize } from 'rxjs/operators';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { AppMessages } from 'src/app/services/app.messages';
import { ModalDetailProductoCategoriaComponent } from './detail/modal-detail-producto-categoria.component';

@Component({
  selector: 'app-producto-categoria',
  templateUrl: './producto-categoria.component.html',
  styleUrls: ['./producto-categoria.component.css']
})
export class ProductoCategoriaComponent implements OnInit {

  public searchProductoCategoria: GenericSearch;
  public paginationProductoCategoria: Pagination;
  public lstProductoCategoria: Array<ProductoCategoria>;

  public estadoAcordion = true;
  public headPage: any;
  
  constructor(
    private productoCategoriaService: ProductoCategoriaService,
    private element: ElementRef,
    private commonsService: CommonsService
  ) { 
    this.searchProductoCategoria = new GenericSearch();
    this.paginationProductoCategoria = new Pagination();
    this.paginationProductoCategoria.createFilters();
    this.lstProductoCategoria = new Array();
  }

  ngOnInit(): void {
    this.searchProductoCategoria.tipo = AppConstants.Field.STRING_NOMBRE;
    this.paginationProductoCategoria.clearFilters();
    this.reqLstPaginado(this.paginationProductoCategoria.page);
  }

  btnBuscarLst(){
    this.paginationProductoCategoria.applyFilterEquals(this.searchProductoCategoria.tipo, this.searchProductoCategoria.dato);
    this.reqLstPaginado();
  }

  btnLimpiar(){
    this.searchProductoCategoria.tipo = AppConstants.Field.STRING_NOMBRE;
    this.searchProductoCategoria.dato = AppConstants.Field.STRING_VACIO;
  }

  btnNuevo(){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_REGISTRAR_PRODUCTO_CATEGORIA,
      operation: AppConstants.Modal.OP_SAVE,
    };
    this.commonsService.openModalLarge(
      ModalDetailProductoCategoriaComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.paginationProductoCategoria.clearFilters();
          this.reqLstPaginado(this.paginationProductoCategoria.page);
        }
      });
  }

  btnDescargarExcel(){}

  btnRefreshLst(){
    this.paginationProductoCategoria.clearFilters();
    this.reqLstPaginado(this.paginationProductoCategoria.page);
  }

  btnDetalle(productoCategoria: ProductoCategoria){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_DETALLE_PRODUCTO_CATEGORIA,
      operation: AppConstants.Modal.OP_DETAIL,
      options: productoCategoria
    };
    this.commonsService.openModalLarge(
      ModalDetailProductoCategoriaComponent,
      params)
      .subscribe((response) => {
      });
  }

  btnEditar(productoCategoria: ProductoCategoria){
    const params = {
      titulo: AppConstants.Modal.TITLE_MODAL_ACTUALIZAR_PRODUCTO_CATEGORIA,
      operation: AppConstants.Modal.OP_UPDATE,
      options: productoCategoria
    };
    this.commonsService.openModalLarge(
      ModalDetailProductoCategoriaComponent,
      params)
      .subscribe((response) => {
        if(response){
          this.commonsService.openModalExitoOperation();
          this.paginationProductoCategoria.clearFilters();
          this.reqLstPaginado(this.paginationProductoCategoria.page);
        }
      });
  }

  async btnBaja(productoCategoria: ProductoCategoria){
    const  a = await this.commonsService.openModalConfirmQuestion();
    if(a.isConfirmed){
      this.commonsService.openModalLoading();
    this.productoCategoriaService.indActivoProductoCategoria(productoCategoria.idProductoCategoria)
        .pipe(
          catchError(error => {
            this.commonsService.openModalErrorServer();
            throw error;
          }),
          finalize(() => {this.commonsService.closeModalLoading()}))
        .subscribe((data) => {
            const response: GenericResponse<any> = data.body;
            switch(response.code){
              case 200:
                this.reqLstPaginado();
                this.commonsService.openModalExitoOperation();
                break;
              default:
                this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
            }
        });
    }
  }

  reqLstPaginado(page?: number){
    this.paginationProductoCategoria.showLoading();
    this.paginationProductoCategoria.onPage(page);
    this.productoCategoriaService.getAll(this.paginationProductoCategoria)
    .pipe(
      catchError(error => {
        console.log('error occured:', error);
        throw error;
      }),
      finalize(() => {
        this.paginationProductoCategoria.onFocus(AppConstants.ID_TABLE_LST_PRODUCTO_CATEGORIA, this.element);
        this.paginationProductoCategoria.hideLoading();
      })
    ).subscribe((data) => {
        this.lstProductoCategoria = data.body.content;
        const total = data.body.totalElements;
        const pagina = (data.body.number)+1;
        this.headPage = {pagina, total};
        this.paginationProductoCategoria.setConfig(this.headPage);
      });
  }

}
