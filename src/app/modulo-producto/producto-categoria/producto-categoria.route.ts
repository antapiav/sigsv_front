import { Route } from '@angular/router';

import { ProductoCategoriaComponent } from './producto-categoria.component';

export const productoCategoriaRoute: Route = {
    path: 'producto-categoria', component: ProductoCategoriaComponent,
    data: {
        title: 'Modulo de Productos - Registro Categoria de producto',
        breadcumb: ['SIGSV', 'Categoria Productos', 'Registro Categoria de producto']
    }
};