import { Component, OnInit, Renderer2, HostListener, Output, EventEmitter, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AppConstants } from 'src/app/util/app.constants';
import { CommonsService } from 'src/app/services/commons/commons.service';
import { SunatReniecService } from 'src/app/services/sunat-reniec/sunat-reniec.service';
import { catchError, finalize, timeout } from 'rxjs/operators';
import { GenericResponse } from 'src/app/model/generic-response.model';
import { AppMessages } from 'src/app/services/app.messages';
import { ProductoCategoria } from 'src/app/model/producto-categoria.model';
import { ProductoCategoriaService } from 'src/app/services/producto/producto-categoria.service';

@Component({
  selector: 'app-modal-detail-producto-categoria',
  templateUrl: './modal-detail-producto-categoria.component.html',
  styleUrls: ['./modal-detail-producto-categoria.component.css']
})
export class ModalDetailProductoCategoriaComponent implements OnInit {
  //variables entrada open modal
  public titulo: string;
  public operation: string;
  public options: any;
  //varaibles modal front
  public disable: boolean = false;
  public disableBtnRegistrar: boolean = false;
  public hideStatus: boolean = false;
  public nameBtnOperation: string;
  //variables propias
  public productoCategoria: ProductoCategoria;
  public response: boolean = false;
  //varaibles form validator
  public productoCategoriaForm: FormGroup;
  public statusValidateNombre = false;
  public statusValidateDetalle= false;


  constructor(
    public bsModalRef: BsModalRef,
    private productoCategoriaService: ProductoCategoriaService,
    private commonsService: CommonsService
  ) {
    this.productoCategoria = new ProductoCategoria();
  }

  ngOnInit() {
    this.formProductoCategoria();
    setTimeout(() => {
      switch(this.operation){
        case AppConstants.Modal.OP_SAVE:
          this.hideStatus = false;
          this.disableBtnRegistrar = true;
          this.productoCategoria.indActivo = true;
          this.nameBtnOperation = AppConstants.Modal.BTN_REGISTRAR;
          this.productoCategoriaForm.controls['indActivo'].disable();
          break;
        case AppConstants.Modal.OP_UPDATE:
          this.getDetalleProductoCategoria(this.options.idProductoCategoria);
          this.hideStatus = false;
          this.disableBtnRegistrar = false;
          this.nameBtnOperation = AppConstants.Modal.BTN_ACTUALIZAR;
          break;
        case AppConstants.Modal.OP_DETAIL:
          this.disable = true;
          this.disableBtnRegistrar = true;
          this.getDetalleProductoCategoria(this.options.idProductoCategoria);
          this.hideStatus = true;
          this.productoCategoriaForm.disable();
          break;
        default:
          this.hideStatus = false
      }
    });
  }

  formProductoCategoria(){
      this.productoCategoriaForm =new FormGroup({
      idProductoCateoria: new FormControl({value: this.productoCategoria.idProductoCategoria, disabled: this.hideStatus}),
      nombre: new FormControl({value: this.productoCategoria.nombre, disabled: this.hideStatus}, [Validators.required]),
      detalle: new FormControl({value: this.productoCategoria.detalle, disabled: this.hideStatus}, [Validators.required]),
      indActivo: new FormControl({value: this.productoCategoria.indActivo, disabled: this.hideStatus})
    });
  }
  //validation formulario
  get f() { return this.productoCategoriaForm.controls; }

  validInput(nameTxt){
    switch(nameTxt){
      case "nombre":
        this.statusValidateNombre = true;
        break;
      case "detalle":
        this.statusValidateDetalle = true;
        break;
    }
  }


  btnRegistrar() {
    if(!this.productoCategoriaForm.invalid){
      this.commonsService.openModalLoading();
      this.productoCategoriaService.postPutOperation(this.productoCategoria, this.operation)
        .pipe(
        catchError(error => {
          this.commonsService.openModalErrorServer();
          throw error;
        }),
        finalize(() => {this.commonsService.closeModalLoading()}))
        .subscribe((data) => {
          const response: GenericResponse<any> = data.body;
          switch(response.code){
            case 200:
              this.response = true;
              this.bsModalRef.hide();
              break;
            case 1:
              this.response = false;
              this.commonsService.openModalInfo(AppMessages.GENERIC_TITLE_OHOH_MODAL, response.message);
              break;
            default:
              this.response = false;
              this.commonsService.openModalErrorOperation(AppMessages.GENERIC_MSG_ERROR_MODAL);
          }
        });
    }
  }

  getDetalleProductoCategoria(idProductoCategoria){
    this.commonsService.openModalLoading();
    this.productoCategoriaService.getProductoCategoria(idProductoCategoria)
      .pipe(
      catchError(error => {
        this.commonsService.openModalErrorServer();
        throw error;
      }),
      finalize(() => {this.commonsService.closeModalLoading()}))
      .subscribe((data) => {
        this.productoCategoria = data.body.body;
        this.formProductoCategoria();
      });
  }

}