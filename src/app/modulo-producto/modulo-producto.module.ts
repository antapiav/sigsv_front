import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CommonsModule } from '../commons/commons.module';

import { SigsvModuloProductoRoutes } from './modulo-producto.route';

import { ProductoComponent } from './producto/producto.component';
import { ProductoCategoriaComponent } from './producto-categoria/producto-categoria.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        AccordionModule,
        CommonsModule,

        SigsvModuloProductoRoutes
    ],
    providers: [],
    declarations: [
        ProductoComponent,
        ProductoCategoriaComponent
    ]
  })
  export class ModuloProductoModule { }