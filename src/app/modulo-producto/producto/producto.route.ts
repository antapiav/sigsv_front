import { Route } from '@angular/router';

import { ProductoComponent } from './producto.component';

export const productoRoute: Route = {
    path: 'producto', component: ProductoComponent,
    data: {
        title: 'Modulo de Productos - Registro de Productos',
        breadcumb: ['SIGSV', 'Productos', 'Registro de Productos']
    }
};