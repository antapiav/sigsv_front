import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { productoRoute } from './producto/producto.route';
import { productoCategoriaRoute } from './producto-categoria/producto-categoria.route';

const MODULO_PRODUCTO_ROUTES = [
    productoRoute,
    productoCategoriaRoute
];

export const moduloProductoRoutes: Routes = MODULO_PRODUCTO_ROUTES;

export const SigsvModuloProductoRoutes: ModuleWithProviders = RouterModule.forChild(moduloProductoRoutes);