import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { SigsvModuloCompraRoutes } from './modulo-compra.route';

import { NuevaCompraComponent } from './nueva/nueva-compra.component';
import { ListaCompraComponent } from './lista/lista-compra.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        ModalModule,
        CollapseModule,
        BsDropdownModule,

        SigsvModuloCompraRoutes
    ],
    providers: [],
    declarations: [
        NuevaCompraComponent,
        ListaCompraComponent
    ]
  })
  export class ModuloCompraModule { }