import { Route } from '@angular/router';

import { NuevaCompraComponent } from './nueva-compra.component';

export const compraRoute: Route = {
    path: 'compra', component: NuevaCompraComponent,
    data: {
        title: 'Modulo de Compras - Nueva Compra',
        breadcumb: ['SIGSV', 'Compras', 'Nueva Compra']
    }
};