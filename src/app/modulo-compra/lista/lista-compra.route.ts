import { Route } from '@angular/router';

import { ListaCompraComponent } from './lista-compra.component';

export const lstCompraRoute: Route = {
    path: 'lst-compra', component: ListaCompraComponent,
    data: {
        title: 'Modulo de Compras - Lista de Compras',
        breadcumb: ['SIGSV', 'Compras', 'Lista de Compras']
    }
};