import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { compraRoute } from './nueva/nueva-compra.route';
import { lstCompraRoute } from './lista/lista-compra.route';

const MODULO_COMPRA_ROUTES = [
    compraRoute,
    lstCompraRoute
];

export const moduloCompraRoutes: Routes = MODULO_COMPRA_ROUTES;

export const SigsvModuloCompraRoutes: ModuleWithProviders = RouterModule.forChild(moduloCompraRoutes);