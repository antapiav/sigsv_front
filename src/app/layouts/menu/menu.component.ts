import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/model/menu';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  collapsed: boolean = true;
  listMenu: Array<Menu>;

  constructor() {
    const listMenuNuevaVenta = [
      new Menu(1, 'Nueva Venta', 'fa-truck', 'modulo-venta/operacion/venta', null)
    ];

    const listMenuLstVenta = [
      new Menu(1, 'Ventas Realizadas', 'fa-truck', 'modulo-venta/operacion/lst-venta', null)
    ];

    const listMenuNuevaCompra = [
      new Menu(1, 'Nueva Compra', 'fa-truck', 'modulo-compra/operacion/compra', null)
    ];

    const listMenuLstCompra = [
      new Menu(1, 'Compra Realizadas', 'fa-truck', 'modulo-compra/operacion/lst-compra', null)
    ];

    const listMenuNuevaPeoductoCategoria = [
      new Menu(1, 'Registrar Categoria', 'fa-truck', 'modulo-producto/operacion/producto-categoria', null)
    ];

    const listMenuNuevaPeoducto = [
      new Menu(1, 'Registrar Producto', 'fa-truck', 'modulo-producto/operacion/producto', null)
    ];

    const listMenuNuevaPoveedorCategoria = [
      new Menu(1, 'Registrar Categoria', 'fa-truck', 'modulo-proveedor/operacion/proveedor-categoria', null)
    ];

    const listMenuNuevaPoveedor = [
      new Menu(1, 'Registrar Proveedor', 'fa-truck', 'modulo-proveedor/operacion/proveedor', null)
    ];

    const listMenuConfiguracionUsuario = [
      new Menu(1, 'Usuarios', 'fa-truck', 'modulo-config/configiracion/usuario', null)
    ];

    const listMenuVentas = [
      new Menu(1, 'Nueva', 'fa-clipboard', 'modulo-venta/operacion', listMenuNuevaVenta),
      new Menu(2, 'Lista de Ventas', 'fa-clipboard', 'modulo-venta/operacion', listMenuLstVenta)
    ];

    const listMenuCompras = [
      new Menu(1, 'Nueva Compra', 'fa-truck', 'modulo-compra/operacion', listMenuNuevaCompra),
      new Menu(2, 'Lista de Compras', 'fa-taxi', 'modulo-compra/operacion', listMenuLstCompra)
    ];

    const listMenuProductos = [
      new Menu(1, 'Categoria', 'fa-truck', 'modulo-producto/operacion', listMenuNuevaPeoductoCategoria),
      new Menu(2, 'Productos', 'fa-taxi', 'modulo-producto/operacion', listMenuNuevaPeoducto)
    ];

    const listMenuProveedores = [
      new Menu(1, 'Categoria', 'fa-truck', 'modulo-proveedor/operacion', listMenuNuevaPoveedorCategoria),
      new Menu(2, 'Proveedores', 'fa-taxi', 'modulo-proveedor/operacion', listMenuNuevaPoveedor)
    ];

    const listMenuClientes = [
      new Menu(1, 'Operaciones Clientes', 'fa-taxi', 'modulo-cliente/cliente', null)
    ];

    const listMenuStock = [
      new Menu(1, 'Operaciones Stock', 'fa-truck', 'modulo-stock/stock', null)
    ];

    const listMenuConfiguracion = [
      new Menu(1, 'Parametricos', 'fa-truck', 'modulo-config/configiracion', null),
      new Menu(2, 'Usuarios', 'fa-truck', 'modulo-config/configiracion', listMenuConfiguracionUsuario)
    ];

    this.listMenu = [
      new Menu(1, ' Ventas', 'fa-table', null, listMenuVentas),
      new Menu(2, ' Compras', 'fa-table', null, listMenuCompras),
      new Menu(3, ' Productos', 'fa-table', null, listMenuProductos),
      new Menu(4, ' Proveedores', 'fa-table', null, listMenuProveedores),
      new Menu(5, ' Clientes', 'fa-table', null, listMenuClientes),
      new Menu(6, ' Stock', 'fa-table', null, listMenuStock),
      new Menu(7, ' Configuraciones', 'fa-table', null, listMenuConfiguracion)
    ];
  }

  ngOnInit() {
  }

}
