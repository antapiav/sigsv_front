import { Component, OnInit } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public today: Observable<number>;

  constructor() {
    this.today = timer(0, 1000).pipe(
      take(Date.now()),
      map(() => Date.now())
    );
  }

  ngOnInit(): void {
  }

  logout() {
  }

}
